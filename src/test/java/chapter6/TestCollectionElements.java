package chapter6;

import app.entities.Man;
import app.entities.ManAddress;
import global.HibernateTest;
import org.hibernate.Session;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;

public class TestCollectionElements extends HibernateTest {

    @BeforeEach
    @AfterEach
    public void cleanUp() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> session.createQuery("from Man", Man.class)
                    .list().forEach(session::remove));
        }
    }

    @Test
    public void shouldCreateManWithAllData() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var phones = Set.of("555555555");
                var address = List.of(new ManAddress("street", "code"));
                var man = new Man("name", "surname", address, phones);
                session.save(man);
            });
        }
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var man = session.createQuery("from Man", Man.class).uniqueResult();
                Assertions.assertEquals(1, man.getAddresses().size());
                Assertions.assertEquals(1, man.getPhones().size());
            });
        }
    }
}
