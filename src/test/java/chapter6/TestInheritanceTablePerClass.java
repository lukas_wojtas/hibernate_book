package chapter6;

import app.entities.TablePerClassInheritanceStrategyChild;
import app.entities.TablePerClassInheritanceStrategyParent;
import global.HibernateTest;
import org.hibernate.Session;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestInheritanceTablePerClass extends HibernateTest {

    @BeforeEach
    @AfterEach
    public void cleanUp() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> session.createQuery("from TablePerClassInheritanceStrategyParent",
                    TablePerClassInheritanceStrategyParent.class).list().forEach(session::remove));
        }
    }

    @Test
    public void shouldCreateParentBook() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> session.save(new TablePerClassInheritanceStrategyParent("title")));
        }
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var book = session.createQuery("from TablePerClassInheritanceStrategyParent",
                        TablePerClassInheritanceStrategyParent.class).uniqueResult();
                Assertions.assertNotNull(book);
                Assertions.assertEquals("title", book.getTitle());
                var itBook = session.createQuery("from TablePerClassInheritanceStrategyChild",
                        TablePerClassInheritanceStrategyChild.class).list();
                Assertions.assertEquals(0, itBook.size());
            });
        }
    }

    @Test
    public void shouldCreateChildBook() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> session.save(new TablePerClassInheritanceStrategyChild("title", "java")));
        }
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var book = session.createQuery("from TablePerClassInheritanceStrategyParent",
                        TablePerClassInheritanceStrategyParent.class).uniqueResult();
                Assertions.assertNotNull(book);
                Assertions.assertEquals("title", book.getTitle());
                var itBook = session.createQuery("from TablePerClassInheritanceStrategyChild",
                        TablePerClassInheritanceStrategyChild.class).uniqueResult();
                Assertions.assertNotNull(itBook);
                Assertions.assertEquals("title", itBook.getTitle());
                Assertions.assertEquals("java", itBook.getLanguage());
            });
        }
    }

    @Test
    public void shouldCreateBothBooks() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                session.save(new TablePerClassInheritanceStrategyParent("title"));
                session.save(new TablePerClassInheritanceStrategyChild("title", "java"));
            });
        }
        try(final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var books = session.createQuery("from TablePerClassInheritanceStrategyParent",
                        TablePerClassInheritanceStrategyParent.class).list();
                Assertions.assertEquals(2, books.size());
                var itBook = session.createQuery("from TablePerClassInheritanceStrategyChild",
                        TablePerClassInheritanceStrategyChild.class).uniqueResult();
                Assertions.assertNotNull(itBook);
                Assertions.assertEquals("title", itBook.getTitle());
                Assertions.assertEquals("java", itBook.getLanguage());
            });
        }
    }


}
