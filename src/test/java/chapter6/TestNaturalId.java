package chapter6;

import app.entities.CompoundNaturalId;
import app.entities.NaturalIdSimpleObject;
import global.HibernateTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestNaturalId extends HibernateTest {

    private Long id;

    @BeforeEach
    @AfterEach
    public void cleanUp() {
        try (final var session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                session.createQuery("from NaturalIdSimpleObject", NaturalIdSimpleObject.class)
                        .list().forEach(session::remove);
                session.createQuery("from CompoundNaturalId", CompoundNaturalId.class)
                        .list().forEach(session::remove);
            });
        }
    }

    @Test
    public void checkLoadingByIdAndNaturalId() {
        try (final var session = sessionFactory.openSession()) {
            this.execute(session, () ->
                    this.id = (Long) session.save(new NaturalIdSimpleObject("sign", 1)));
        }
        try (final var session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var object = session.byId(NaturalIdSimpleObject.class).load(this.id);
                Assertions.assertNotNull(object);
                Assertions.assertEquals("sign", object.getKey());
                Assertions.assertEquals(1, object.getValue());
            });
        }
        try (final var session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var object = session.bySimpleNaturalId(NaturalIdSimpleObject.class)
                        .load("sign");
                Assertions.assertNotNull(object);
                Assertions.assertEquals(this.id, object.getId());
                Assertions.assertEquals(1, object.getValue());
            });
        }
    }

    @Test
    public void checkLoadingByCompoundNaturalId() {
        try (final var session = sessionFactory.openSession()) {
            this.execute(session, () -> session.persist(new CompoundNaturalId("key1", "key2", "value")));
        }
        try (final var session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var object = session.byNaturalId(CompoundNaturalId.class)
                        .using("compoundId1", "key1")
                        .using("compoundId2", "key2").load();
                Assertions.assertNotNull(object);
                Assertions.assertEquals("value", object.getValue());
            });
        }
    }

    @Test
    public void checkIfExceptionThrownWhenKeyNotExistWithLoad() {
        try (final var session = sessionFactory.openSession()) {
            this.execute(session, () -> session.persist(new CompoundNaturalId("key1", "key2", "value")));
        }
        try (final var session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var object = session.byNaturalId(CompoundNaturalId.class)
                        .using("compoundId1", "key1")
                        .using("compoundId2", "key22").load();
                Assertions.assertNull(object);
            });
        }
    }

    @Test
    public void checkHowGetReferenceWorks() {
        try (final var session = sessionFactory.openSession()) {
            this.execute(session, () -> session.persist(new CompoundNaturalId("key1", "key2", "value")));
        }
        try (final var session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var object = session.byNaturalId(CompoundNaturalId.class)
                        .using("compoundId1", "key1")
                        .using("compoundId2", "key2").getReference();
                Assertions.assertNotNull(object);
                Assertions.assertEquals("value", object.getValue());
            });
        }
    }

    @Test
    public void checkIdReferenceThrowExceptionWhenKeyNonExist() {
        try (final var session = sessionFactory.openSession()) {
            this.execute(session, () -> session.persist(new CompoundNaturalId("key1", "key1", "value")));
        }
        try (final var session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var object = session.byNaturalId(CompoundNaturalId.class)
                        .using("compoundId1", "key2")
                        .using("compoundId2", "key1")
                        .getReference();
                Assertions.assertNull(object);
            });
        }
    }
}
