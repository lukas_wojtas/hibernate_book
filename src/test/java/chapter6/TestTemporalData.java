package chapter6;

import app.entities.TemporalData;
import global.HibernateTest;
import org.hibernate.Session;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Calendar;
import java.util.Date;

public class TestTemporalData extends HibernateTest {

    @BeforeEach
    @AfterEach
    public void cleanUp() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> session.createQuery("from TemporalData", TemporalData.class)
                    .list().forEach(session::remove));
        }
    }

    @Test
    public void shouldCreateTemporalDataRecord() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var calendar = Calendar.getInstance();
                calendar.add(Calendar.MONTH, 4);
                session.save(new TemporalData(calendar, new Date(), new Date()));
            });
        }
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> Assertions.assertNotNull(
                    session.createQuery("from TemporalData", TemporalData.class).uniqueResult()));
        }
    }
}
