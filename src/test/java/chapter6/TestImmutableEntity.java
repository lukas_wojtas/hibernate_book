package chapter6;

import app.entities.ImmutablePerson;
import global.HibernateTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestImmutableEntity extends HibernateTest {

    @BeforeEach
    @AfterEach
    public void cleanUp() {
        try (final var session = sessionFactory.openSession()) {
            this.execute(session, () -> session.createQuery("from ImmutablePerson", ImmutablePerson.class)
                    .list().forEach(session::remove));
        }
    }

    @Test
    public void testIfImmutableEntityIsNotChanging() {
        try (final var session = sessionFactory.openSession()) {
            this.execute(session, () -> session.save(new ImmutablePerson("immutable")));
        }
        try (final var session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var person = session.createQuery("from ImmutablePerson p where p.name=:name", ImmutablePerson.class)
                        .setParameter("name", "immutable").uniqueResult();
                person.setName("new name");
                session.save(person);
                session.refresh(person);
                Assertions.assertEquals("immutable", person.getName());
            });
        }
    }
}
