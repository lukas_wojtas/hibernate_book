package chapter6;

import app.entities.PersonWithNamedQuery;
import global.HibernateTest;
import org.hibernate.Session;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestNamedQuery extends HibernateTest {

    @BeforeEach
    @AfterEach
    public void cleanUp() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> session.createQuery("from PersonWithNamedQuery", PersonWithNamedQuery.class)
                    .list().forEach(session::remove));
        }
    }

    @Test
    public void shouldExecuteNamedQuery() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> session.save(new PersonWithNamedQuery("name")));
        }
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                Assertions.assertNotNull(
                        session.createNamedQuery("findByName", PersonWithNamedQuery.class)
                                .setParameter("name", "name")
                                .uniqueResult());
                var person = session.createNamedQuery("findAll", PersonWithNamedQuery.class)
                        .uniqueResult();
                Assertions.assertNotNull(person);
                Assertions.assertNotNull(session.createNamedQuery("findById", PersonWithNamedQuery.class)
                        .setParameter("id", person.getId())
                        .uniqueResult());
            });
        }
    }
}
