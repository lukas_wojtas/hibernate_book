package chapter6;

import app.entities.SimpleObjectWithBasic;
import global.HibernateTest;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
/*
* Support for LAZY fetching on Basic is optional in JPA, so some JPA providers may not support it.
* */
public class TestBasicDecorator extends HibernateTest {

    @BeforeEach
    @AfterEach
    public void setUp() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> session.createQuery("from SimpleObjectWithBasic", SimpleObjectWithBasic.class)
                    .list().forEach(session::delete));
        }
    }

    @Test
    public void shouldGetObjectWithoutHibernateProxyInitialized() {
        try (final Session session = sessionFactory.openSession()) {
            session.save(new SimpleObjectWithBasic("key", 33));
        }
        try (final Session session = sessionFactory.openSession()) {
            SimpleObjectWithBasic basic = session.createQuery("from SimpleObjectWithBasic", SimpleObjectWithBasic.class)
                    .uniqueResult();
            Assertions.assertTrue(Hibernate.isInitialized(basic.getKey()));
            Assertions.assertTrue(Hibernate.isInitialized(basic.getValue()));
        }
    }
}
