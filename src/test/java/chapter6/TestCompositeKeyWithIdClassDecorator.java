package chapter6;

import app.entities.UserCompositeKey3;
import global.HibernateTest;
import org.hibernate.NonUniqueObjectException;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestCompositeKeyWithIdClassDecorator extends HibernateTest {

    @AfterEach
    @BeforeEach
    public void tearDown() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> session.createQuery("from UserCompositeKey3", UserCompositeKey3.class)
                    .list().forEach(session::remove));
        }
    }

    @Test
    public void shouldCreateUserWithCompositeKeyInExternalClass() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                UserCompositeKey3 user = new UserCompositeKey3("firstName", "lastName", 33);
                session.save(user);
            });
        }
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                Query<UserCompositeKey3> query = session.createQuery("from UserCompositeKey3 u " +
                                "where u.name=:name and u.surname=:surname",
                        UserCompositeKey3.class);
                query.setParameter("name", "firstName");
                query.setParameter("surname", "lastName");
                Assertions.assertNotNull(query.uniqueResult());
            });
        }
    }

    @Test
    public void shouldDetectSameCompositeKey() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                UserCompositeKey3 user1 = new UserCompositeKey3("firstName", "lastName", 33);
                UserCompositeKey3 user2 = new UserCompositeKey3("firstName", "lastName", 23);
                session.save(user1);
                Assertions.assertThrows(NonUniqueObjectException.class, () -> session.save(user2));
            });
        }
    }

    @Test
    public void shouldAllowToCreateUsersWithDifferentPrimaryKey() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                UserCompositeKey3 user1 = new UserCompositeKey3("name", "surname", 33);
                UserCompositeKey3 user2 = new UserCompositeKey3("name1", "surname", 33);
                UserCompositeKey3 user3 = new UserCompositeKey3("name", "surname1", 33);
                session.save(user1);
                session.save(user2);
                session.save(user3);
            });
        }
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> Assertions.assertEquals(3,
                    session.createQuery("from UserCompositeKey3", UserCompositeKey3.class)
                            .list()
                            .size()));
        }
    }
}
