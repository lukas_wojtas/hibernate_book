package chapter6;

import app.entities.SingleTableInheritanceBasicBook;
import app.entities.SingleTableInheritanceItBook;
import global.HibernateTest;
import org.hibernate.Session;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestInheritanceSingleTable extends HibernateTest {

    @BeforeEach
    @AfterEach
    public void clearUp() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                session.createQuery("from SingleTableInheritanceBasicBook", SingleTableInheritanceBasicBook.class)
                        .list().forEach(session::remove);
                session.createQuery("from SingleTableInheritanceItBook", SingleTableInheritanceItBook.class)
                        .list().forEach(session::remove);
            });
        }
    }

    @Test
    public void shouldCreateBasicBook() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> session.save(new SingleTableInheritanceBasicBook("title")));
        }
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var book = session.createQuery("from SingleTableInheritanceBasicBook", SingleTableInheritanceBasicBook.class)
                        .uniqueResult();
                Assertions.assertNotNull(book);
                Assertions.assertEquals("title", book.getTitle());
            });
        }
    }

    @Test
    public void shouldCreateItBook() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> session.save(new SingleTableInheritanceItBook("title", "java")));
        }
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var book = session.createQuery("from SingleTableInheritanceItBook", SingleTableInheritanceItBook.class)
                        .uniqueResult();
                Assertions.assertEquals("title", book.getTitle());
                Assertions.assertEquals("java", book.getLanguage());
            });
        }
    }

    @Test
    public void shouldCreateBothTypesOfBooks() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                session.save(new SingleTableInheritanceBasicBook("title"));
                session.save(new SingleTableInheritanceItBook("title", "java"));
            });
        }
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var bookIt = session.createQuery("from SingleTableInheritanceItBook", SingleTableInheritanceItBook.class)
                        .uniqueResult();
                Assertions.assertEquals("title", bookIt.getTitle());
                Assertions.assertEquals("java", bookIt.getLanguage());
                var bookBasic = session.createQuery("from SingleTableInheritanceBasicBook", SingleTableInheritanceBasicBook.class)
                        .list();
                Assertions.assertEquals(2, bookBasic.size());
            });
        }
    }
}
