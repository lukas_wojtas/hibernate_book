package chapter6;

import app.entities.UserDetails;
import app.entities.UserWithSecondaryTable;
import global.HibernateTest;
import org.hibernate.Session;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestUseOfSecondaryTable extends HibernateTest {

    @BeforeEach
    @AfterEach
    public void tearDown() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> session.createQuery("from UserWithSecondaryTable",
                    UserWithSecondaryTable.class)
                    .list().forEach(session::remove));
        }
    }

    @Test
    public void shouldCreateUserAndUserDetails() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                UserWithSecondaryTable user = new UserWithSecondaryTable("name33", 33, "surname");
                session.save(user);
            });
        }
        try (final Session session = sessionFactory.openSession()) {
            UserDetails details = session.createQuery("from UserDetails", UserDetails.class).uniqueResult();
            UserWithSecondaryTable user = session.createQuery("from UserWithSecondaryTable",
                    UserWithSecondaryTable.class).uniqueResult();
            Assertions.assertNotNull(details);
            Assertions.assertNotNull(user);
            Assertions.assertEquals("name33", details.getName());
            Assertions.assertEquals("surname", details.getSurname());
            Assertions.assertEquals(33, details.getAge());
            Assertions.assertEquals("name33", user.getName());
            Assertions.assertEquals(33, user.getAge());
            Assertions.assertEquals("surname", user.getSurname());
        }
    }
}
