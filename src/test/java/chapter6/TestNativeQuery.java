package chapter6;

import app.entities.PersonWithNativeQuery;
import global.HibernateTest;
import org.hibernate.Session;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestNativeQuery extends HibernateTest {

    @SuppressWarnings("unchecked")
    @BeforeEach
    @AfterEach
    public void cleanUp() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> session.createNamedQuery("findAllPeople").list().forEach(session::remove));
        }
    }

    @Test
    public void shouldCreatePersonWithNativeQuery() {
        try(final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> session.save(new PersonWithNativeQuery("name")));
        }
        try(final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> Assertions.assertEquals(1, session.createNamedQuery("findAllPeople").list().size()));
        }
    }
}
