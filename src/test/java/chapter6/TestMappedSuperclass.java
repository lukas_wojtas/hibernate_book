package chapter6;

import app.entities.MappedChildClass;
import global.HibernateTest;
import org.hibernate.Session;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestMappedSuperclass extends HibernateTest {

    @BeforeEach
    @AfterEach
    public void cleanUp() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> session.createQuery("from MappedChildClass", MappedChildClass.class)
                    .list().forEach(session::remove));
        }
    }

    @Test
    public void shouldCreateMappedChildClassWhichInheritMappedParentClass() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> session.save(new MappedChildClass("parent", "child")));
        }
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> Assertions.assertNotNull(session.createQuery("from MappedChildClass", MappedChildClass.class).uniqueResult()));
        }
    }
}
