package chapter6;

import app.entities.EmbeddedDetails;
import app.entities.UserWithEmbeddedDetails;
import global.HibernateTest;
import org.hibernate.Session;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestFullyEmbeddableUser extends HibernateTest {

    @BeforeEach
    @AfterEach
    public void setUp() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                session.createQuery("from UserWithEmbeddedDetails", UserWithEmbeddedDetails.class)
                        .list().forEach(session::remove);
            });
        }
    }

    @Test
    public void shouldCreateEmbeddedUser() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var details = new EmbeddedDetails("name", "surname");
                var user = new UserWithEmbeddedDetails(33, details);
                session.save(user);
            });
        }

        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var user = session.createQuery("from UserWithEmbeddedDetails", UserWithEmbeddedDetails.class)
                        .uniqueResult();
                Assertions.assertEquals("name", user.getDetails().getName());
                Assertions.assertEquals("surname", user.getDetails().getSurname());
            });
        }
    }
}
