package chapter6;

import app.entities.Sequence;
import global.HibernateTest;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.Serializable;

public class TestSequenceGenerator extends HibernateTest {

    @AfterEach
    @BeforeEach
    public void tearDown() {
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            session.createQuery("from Sequence", Sequence.class).list().forEach(session::delete);
            tx.commit();
        }
    }

    @Test
    public void shouldCreateSeqObject() {
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            Sequence sequence = new Sequence("value");
            Serializable id = session.save(sequence);
            Assertions.assertNotNull(id);
            tx.commit();
        }
    }
}
