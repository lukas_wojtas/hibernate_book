package chapter6;

import app.entities.BookWithPublicationDate;
import global.HibernateTest;
import org.hibernate.Session;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

public class TestTransientDecorator extends HibernateTest {

    @BeforeEach
    @AfterEach
    public void setUp() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> session.createQuery("from BookWithPublicationDate", BookWithPublicationDate.class)
                    .list().forEach(session::remove));
        }
    }

    @Test
    public void shouldCreateBookWithTransientDecorator() {
        try (final Session session = sessionFactory.openSession()) {
            final var book = new BookWithPublicationDate("title", LocalDate.now());
            this.execute(session, () -> {
                session.save(new BookWithPublicationDate("title", LocalDate.now()));
                Assertions.assertNotNull(book.getPublicationDate());
            });
        }
        try (final Session session = sessionFactory.openSession()) {
            var book =
                    session.createQuery("from BookWithPublicationDate", BookWithPublicationDate.class).uniqueResult();
            Assertions.assertNotNull(book.getId());
            Assertions.assertNotNull(book.getTitle());
            //No library set so no relation
            Assertions.assertNull(book.getLibrary());
            //Date is transient so no null in read
            Assertions.assertNull(book.getPublicationDate());
        }
    }
}
