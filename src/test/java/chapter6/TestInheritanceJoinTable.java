package chapter6;

import app.entities.JoinTableInheritanceStrategyChild;
import app.entities.JoinTableInheritanceStrategyParent;
import global.HibernateTest;
import org.hibernate.Session;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestInheritanceJoinTable extends HibernateTest {

    @BeforeEach
    @AfterEach
    public void cleanUp() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> session.createQuery("from JoinTableInheritanceStrategyParent", JoinTableInheritanceStrategyParent.class)
                    .list().forEach(session::remove));
        }
    }

    @Test
    public void shouldCreateParentBook() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> session.save(new JoinTableInheritanceStrategyParent("title")));
        }
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var book = session.createQuery("from JoinTableInheritanceStrategyParent", JoinTableInheritanceStrategyParent.class)
                        .uniqueResult();
                Assertions.assertNotNull(book);
                Assertions.assertEquals("title", book.getTitle());
            });
        }
    }

    @Test
    public void shouldCreateChildBook() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> session.save(new JoinTableInheritanceStrategyChild("title", "java")));
        }
        try (Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var javaBook = session.createQuery("from JoinTableInheritanceStrategyChild",
                        JoinTableInheritanceStrategyChild.class).uniqueResult();
                Assertions.assertNotNull(javaBook);
                Assertions.assertEquals("title", javaBook.getTitle());
                Assertions.assertEquals("java", javaBook.getLanguage());
            });
        }
    }

    @Test
    public void shouldCreateBothBooks() {
        try(final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                session.save(new JoinTableInheritanceStrategyChild("title", "java"));
                session.save(new JoinTableInheritanceStrategyParent("title"));
            });
        }
        try(final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var books = session.createQuery("from JoinTableInheritanceStrategyParent",
                        JoinTableInheritanceStrategyParent.class).list();
                Assertions.assertEquals(2, books.size());
                var itBook = session.createQuery("from JoinTableInheritanceStrategyChild",
                        JoinTableInheritanceStrategyChild.class).uniqueResult();
                Assertions.assertNotNull(itBook);
                Assertions.assertEquals("title", itBook.getTitle());
                Assertions.assertEquals("java", itBook.getLanguage());
            });
        }
    }
}
