package chapter6;

import app.entities.Human;
import app.entities.Pet;
import global.HibernateTest;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class TestManyToOne extends HibernateTest {

    @BeforeEach
    @AfterEach
    public void cleanUp() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                session.createQuery("from Human", Human.class)
                        .list().forEach(session::remove);
                session.createQuery("from Pet", Pet.class)
                        .list().forEach(session::remove);
            });
        }
    }

    @Test
    public void shouldCreateHumanWithDogAndCat() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var human = new Human("man");
                var cat = new Pet("cat");
                cat.setHuman(human);
                var dog = new Pet("dog");
                dog.setHuman(human);
                var pets = Arrays.asList(cat, dog);
                human.setPets(pets);
                session.save(human);
            });
        }
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var human = session.createQuery("from Human", Human.class).uniqueResult();
                Assertions.assertFalse(Hibernate.isInitialized(human.getPets()));
                var pets = human.getPets();
                Assertions.assertEquals(2, pets.size());
                Assertions.assertEquals("cat", pets.get(0).getName());
                Assertions.assertEquals("dog", pets.get(1).getName());
                pets = session.createQuery("from Pet", Pet.class).list();
                Assertions.assertEquals(2, pets.size());
            });
        }
    }

    @Test
    public void shouldCreateHumanWithoutPet() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> session.save(new Human("human")));
        }
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var human = session.createQuery("from Human", Human.class).uniqueResult();
                Assertions.assertEquals("human", human.getName());
                Assertions.assertTrue(human.getPets().isEmpty());
            });
        }
    }

    @Test
    public void shouldCreatePetWithoutHuman() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> session.save(new Pet("tortoise")));
        }
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                Assertions.assertTrue(session.createQuery("from Human", Human.class).list().isEmpty());
                Assertions.assertEquals("tortoise", session.createQuery("from Pet", Pet.class).uniqueResult().getName());
            });
        }
    }

    @Test
    public void shouldRemoveOrphansFromHuman() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var human = new Human("man");
                var cat = new Pet("cat");
                cat.setHuman(human);
                var dog = new Pet("dog");
                dog.setHuman(human);
                var pets = Arrays.asList(cat, dog);
                human.setPets(pets);
                session.save(human);
            });
        }
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var dog = session
                        .createQuery("from Pet p where p.name=:name", Pet.class)
                        .setParameter("name", "dog").uniqueResult();
                session.remove(dog);
            });
        }
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () ->
                    Assertions.assertEquals(1, session.createQuery("from Human", Human.class)
                            .uniqueResult().getPets().size()));
        }
    }
}
