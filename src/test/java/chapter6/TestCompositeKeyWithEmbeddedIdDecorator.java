package chapter6;

import app.entities.UserCompositeId2;
import app.entities.UserCompositeKey2;
import global.HibernateTest;
import org.hibernate.NonUniqueObjectException;
import org.hibernate.Session;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestCompositeKeyWithEmbeddedIdDecorator extends HibernateTest {

    @AfterEach
    @BeforeEach
    public void tearDown() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                session.createQuery("from UserCompositeKey2", UserCompositeKey2.class)
                        .list().forEach(session::remove);
            });
        }
    }

    @Test
    public void shouldCreateUserWithCompositeKeyInExternalClass() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                UserCompositeId2 id2 = new UserCompositeId2("firstName", "lastName");
                UserCompositeKey2 user = new UserCompositeKey2(id2, 33);
                session.save(user);
            });
        }
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                Assertions.assertNotNull(session.createQuery("from UserCompositeKey2", UserCompositeKey2.class)
                        .uniqueResult());
            });
        }
    }

    @Test
    public void shouldDetectSameCompositeKey() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                UserCompositeId2 id1 = new UserCompositeId2("firstName", "lastName");
                UserCompositeId2 id2 = new UserCompositeId2("firstName", "lastName");
                UserCompositeKey2 user1 = new UserCompositeKey2(id1, 33);
                UserCompositeKey2 user2 = new UserCompositeKey2(id2, 33);
                session.save(user1);
                Assertions.assertThrows(NonUniqueObjectException.class, () -> session.save(user2));
            });
        }
    }

    @Test
    public void shouldAllowToCreateUsersWithDifferentPrimaryKey() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                UserCompositeId2 id1 = new UserCompositeId2("firstName", "lastName");
                UserCompositeId2 id2 = new UserCompositeId2("firstName1", "lastName");
                UserCompositeId2 id3 = new UserCompositeId2("firstName", "lastName1");
                UserCompositeKey2 user1 = new UserCompositeKey2(id1, 33);
                UserCompositeKey2 user2 = new UserCompositeKey2(id2, 33);
                UserCompositeKey2 user3 = new UserCompositeKey2(id3, 33);
                session.save(user1);
                session.save(user2);
                session.save(user3);
            });
        }
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                Assertions.assertEquals(3, session.createQuery("from UserCompositeKey2", UserCompositeKey2.class).list().size());
            });
        }
    }
}
