package chapter6;

import app.entities.HumanOrderColumn;
import app.entities.Pet;
import app.entities.PetOrderColumn;
import global.HibernateTest;
import org.hibernate.Session;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

public class TestOrderBy extends HibernateTest {

    @BeforeEach
    @AfterEach
    public void setUp() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                session.createQuery("from HumanOrderColumn", HumanOrderColumn.class)
                        .list().forEach(session::remove);
                session.createQuery("from Pet", Pet.class).list().forEach(session::remove);
            });
        }
    }

    @Test
    public void shouldCreateHumanWithOrderOfPets() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var human = new HumanOrderColumn("human");
                var pets = List.of(new PetOrderColumn("snake"), new PetOrderColumn("turtle"), new PetOrderColumn("bird"));
                pets.forEach(p -> p.setHuman(human));
                human.setPets(pets);
                session.save(human);
            });
        }
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var human = session.createQuery("from HumanOrderColumn", HumanOrderColumn.class)
                        .uniqueResult();
                Assertions.assertEquals(3, human.getPets().size());
                Assertions.assertEquals(0, human.getPets().get(0).getId());
                Assertions.assertEquals(1, human.getPets().get(1).getId());
                Assertions.assertEquals(2, human.getPets().get(2).getId());
            });
        }
    }
}
