package chapter6;

import app.entities.UserCompositeId;
import app.entities.UserCompositeKey;
import global.HibernateTest;
import org.hibernate.NonUniqueObjectException;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestCompositeKeyWithIdAndEmbeddedDecorators extends HibernateTest {

    @AfterEach
    @BeforeEach
    public void tearDown() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> session.createQuery("from UserCompositeKey", UserCompositeKey.class)
                    .list().forEach(session::delete));
        }
    }

    @Test
    public void shouldCreateUserWithCompositeKeyInExternalClass() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                UserCompositeId id = new UserCompositeId("firstName", "lastName");
                UserCompositeKey user = new UserCompositeKey(id, 33);
                session.save(user);
            });
        }
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                Query<UserCompositeKey> query = session.createQuery("from UserCompositeKey u " +
                                "where u.compositeId.name=:name and u.compositeId.surname=:surname",
                        UserCompositeKey.class);
                query.setParameter("name", "firstName");
                query.setParameter("surname", "lastName");
                Assertions.assertNotNull(query.uniqueResult());
            });
        }
    }

    @Test
    public void shouldDetectSameCompositeKey() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                UserCompositeId id1 = new UserCompositeId("firstName", "lastName");
                UserCompositeId id2 = new UserCompositeId("firstName", "lastName");
                UserCompositeKey user1 = new UserCompositeKey(id1, 33);
                UserCompositeKey user2 = new UserCompositeKey(id2, 23);
                session.save(user1);
                Assertions.assertThrows(NonUniqueObjectException.class, () -> session.save(user2));
            });
        }
    }

    @Test
    public void shouldAllowToCreateUsersWithDifferentPrimaryKey() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                UserCompositeId id1 = new UserCompositeId("firstName", "lastName");
                UserCompositeId id2 = new UserCompositeId("firstName1", "lastName");
                UserCompositeId id3 = new UserCompositeId("firstName", "lastName1");
                UserCompositeKey user1 = new UserCompositeKey(id1, 33);
                UserCompositeKey user2 = new UserCompositeKey(id2, 33);
                UserCompositeKey user3 = new UserCompositeKey(id3, 33);
                session.save(user1);
                session.save(user2);
                session.save(user3);
            });
        }
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                Query<UserCompositeKey> query = session.createQuery("from UserCompositeKey u " +
                                "where u.compositeId.name=:name and u.compositeId.surname=:surname",
                        UserCompositeKey.class);
                query.setParameter("name", "firstName");
                query.setParameter("surname", "lastName");
                Assertions.assertNotNull(query.uniqueResult());
                query = session.createQuery("from UserCompositeKey", UserCompositeKey.class);
                Assertions.assertEquals(3, query.list().size());
            });
        }
    }
}
