package chapter6;

import app.entities.Article;
import app.entities.Newspaper;
import global.HibernateTest;
import org.hibernate.Session;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class TestManyToMany extends HibernateTest {

    @BeforeEach
    @AfterEach
    public void cleanUp() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                session.createQuery("from Newspaper", Newspaper.class).list().forEach(session::remove);
                session.createQuery("from Article", Article.class).list().forEach(session::remove);
            });
        }
    }

    @Test
    public void shouldCreateNewspaperWithTwoArticles() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var newspaper = new Newspaper("times");
                var article1 = new Article("title1");
                var article2 = new Article("title 2");
                newspaper.setArticles(new HashSet<>(Arrays.asList(article1, article2)));
                session.save(newspaper);
            });
        }
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var newspaper = session.createQuery("from Newspaper", Newspaper.class).uniqueResult();
                Assertions.assertEquals("times", newspaper.getTitle());
                Assertions.assertEquals(2, newspaper.getArticles().size());
                session.createQuery("from Article", Article.class).list().forEach(article -> {
                    Assertions.assertEquals(1, article.getNewspapers().size());
                    article.getNewspapers().forEach(paper -> Assertions.assertEquals("times", paper.getTitle()));
                });

            });
        }
    }

    @Test
    public void shouldCreateArticleInTwoNewspapers() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var article = new Article("art 1");
                var paper1 = new Newspaper("Times1");
                var paper2 = new Newspaper("Times2");
                paper1.setArticles(new HashSet<>(Collections.singletonList(article)));
                paper2.setArticles(new HashSet<>(Collections.singletonList(article)));
                session.save(paper1);
                session.save(paper2);
            });
        }
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var articles = session.createQuery("from Newspaper", Newspaper.class).list();
                Assertions.assertEquals(2, articles.size());
                articles.forEach(paper -> {
                    Assertions.assertEquals(1, paper.getArticles().size());
                    Assertions.assertEquals("art 1", List.copyOf(paper.getArticles()).get(0).getTitle());
                });
            });
        }
    }

    @Test
    public void shouldCreateNewspaperWithoutArticle() {
        try(final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var article = new Article("article");
                session.save(article);
            });
        }
        try(final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> Assertions.assertNotNull(session.createQuery("from Article", Article.class).uniqueResult()));
        }
    }

    @Test
    public void shouldCreateArticleWithoutNewspaper() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var article = new Article("aaaa");
                session.save(article);
            });
        }
        try(final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> Assertions.assertNotNull(session.createQuery("from Article", Article.class).uniqueResult()));
        }
    }

}
