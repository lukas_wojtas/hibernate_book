package chapter3;

import app.entities.Person;
import app.entities.Ranking;
import app.entities.Skill;
import app.gateways.PersonDataGateway;
import app.gateways.RankingDataGateway;
import app.gateways.SkillDataGateway;
import global.HibernateTest;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

public class TestRankingDataGateway extends HibernateTest {

    private RankingDataGateway rankingGateway;
    private PersonDataGateway personGateway;
    private SkillDataGateway skillGateway;

    @BeforeEach
    public void setUpGateways() {
        this.rankingGateway = new RankingDataGateway();
        this.personGateway = new PersonDataGateway();
        this.skillGateway = new SkillDataGateway();
    }

    @Test
    public void testPersistRanking() {
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            this.rankingGateway.deleteRanking(session,
                    rankingGateway.getRankingBySubjectAndSkill(session, "subject", "reading"));
            final Person subject = this.personGateway.getPeopleByName(session, "subject")
                    .stream()
                    .findFirst()
                    .orElseGet(() -> {
                        var p = new Person("subject");
                        this.personGateway.persistPersonWithUniqueName(session, p);
                        return p;
                    });
            final Person observer = this.personGateway.getPeopleByName(session, "observer")
                    .stream()
                    .findFirst()
                    .orElseGet(() -> {
                        var p = new Person("observer");
                        this.personGateway.persistPersonWithUniqueName(session, p);
                        return p;
                    });
            final Skill skill = this.skillGateway.getSkillByName(session, "reading")
                    .orElse(new Skill("reading"));
            final Ranking ranking = new Ranking(subject, observer, skill, 5);
            this.rankingGateway.persistRanking(session, ranking);
            tx.commit();
            tx = session.beginTransaction();
            final Ranking ranking1 = this.rankingGateway.getRankingBySubjectAndObserverAndSkill(session, "subject",
                    "observer", "reading").orElseThrow();
            Assertions.assertEquals(ranking, ranking1);
            this.rankingGateway.deleteRanking(session,
                    this.rankingGateway.getRankingBySubjectAndSkill(session, "subject", "reading"));
            tx.commit();
        }
    }

    @Test
    public void shouldReadAllRankingWithGivenSubjectAndSkillName() {
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            this.rankingGateway.deleteRanking(session,
                    rankingGateway.getRankingBySubjectAndSkill(session, "subject", "reading"));
            final Person subject = this.personGateway.getPeopleByName(session, "subject")
                    .stream()
                    .findFirst()
                    .orElseGet(() -> {
                        var p = new Person("subject");
                        this.personGateway.persistPersonWithUniqueName(session, p);
                        return p;
                    });
            final Person observer = this.personGateway.getPeopleByName(session, "observer")
                    .stream()
                    .findFirst()
                    .orElseGet(() -> {
                        var p = new Person("observer");
                        this.personGateway.persistPersonWithUniqueName(session, p);
                        return p;
                    });
            final Skill skill = this.skillGateway.getSkillByName(session, "reading")
                    .orElse(new Skill("reading"));
            final Ranking ranking = new Ranking(subject, observer, skill, 5);
            final Ranking ranking1 = new Ranking(subject, observer, skill, 5);
            this.rankingGateway.saveRanking(session, ranking);
            this.rankingGateway.saveRanking(session, ranking1);
            List<Ranking> rankings = rankingGateway
                    .getRankingBySubjectAndSkill(session, "subject", "reading");
            Assertions.assertEquals(2, rankings.size());
            tx.commit();
        }
    }

    @Test
    public void testUpdateOfRanking() {
        try (final Session session = sessionFactory.openSession()) {
            //First transaction
            Transaction tx = session.beginTransaction();
            this.rankingGateway.deleteRanking(session,
                    this.rankingGateway.getRankingBySubjectAndSkill(session, "subject", "reading"));
            final Person subject = this.personGateway.getPeopleByName(session, "subject")
                    .stream()
                    .findFirst()
                    .orElseGet(() -> {
                        var p = new Person("subject");
                        session.persist(p);
                        return p;
                    });
            final Person observer = this.personGateway.getPeopleByName(session, "observer")
                    .stream()
                    .findFirst()
                    .orElseGet(() -> {
                        var p = new Person("observer");
                        session.persist(p);
                        return p;
                    });
            final Skill skill = this.skillGateway.getSkillByName(session, "reading").orElse(new Skill("reading"));
            rankingGateway.saveRanking(session, new Ranking(subject, observer, skill, 4));
            tx.commit();
            //Second transaction
            tx = session.beginTransaction();
            Ranking ranking = this.rankingGateway
                    .getRankingBySubjectAndObserverAndSkill(session, "subject",
                            "observer", "reading").orElseThrow(RuntimeException::new);
            Assertions.assertEquals(4, ranking.getRanking());
            ranking.setRanking(33);
            tx.commit();
            //third transaction
            tx = session.beginTransaction();
            ranking = this.rankingGateway
                    .getRankingBySubjectAndObserverAndSkill(session, "subject",
                            "observer", "reading").orElseThrow(RuntimeException::new);
            Assertions.assertEquals(33, ranking.getRanking());
            this.rankingGateway.deleteRanking(session,
                    this.rankingGateway.getRankingBySubjectAndSkill(session, "subject", "reading"));
            tx.commit();
        }
    }

    @Test
    public void shouldCheckAverage() {
        try (Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            this.rankingGateway.getRankingBySubjectAndSkill(session, "subject", "reading");
            this.rankingGateway.deleteRanking(session,
                    this.rankingGateway.getRankingBySubjectAndSkill(session, "subject", "reading"));
            Assertions.assertEquals(0.0, this.rankingGateway.getAverageRankingForSubject(session, "subject", "reading"));
            Person subject = this.personGateway.getPeopleByName(session, "subject")
                    .stream()
                    .findFirst()
                    .orElse(new Person("subject"));
            Person observer = this.personGateway.getPeopleByName(session, "observer")
                    .stream()
                    .findFirst()
                    .orElse(new Person("observer"));
            Skill skill = this.skillGateway.getSkillByName(session, "reading").orElse(new Skill("reading"));
            Ranking r1 = new Ranking(subject, observer, skill, 3);
            Ranking r2 = new Ranking(subject, observer, skill, 4);
            Ranking r3 = new Ranking(subject, observer, skill, 5);
            this.rankingGateway.saveRanking(session, r1);
            this.rankingGateway.saveRanking(session, r2);
            this.rankingGateway.saveRanking(session, r3);
            tx.commit();
            tx = session.beginTransaction();
            Assertions.assertEquals(4.0,
                    this.rankingGateway.getAverageRankingForSubject(session, "subject", "reading"));
            this.rankingGateway.deleteRanking(session,
                    this.rankingGateway.getRankingBySubjectAndSkill(session, "subject", "reading"));
            tx.commit();
        }
    }

    @Test
    public void testDeleteEntity() {
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            List<Ranking> toDelete = this.rankingGateway.getRankingBySubjectAndSkill(session, "testSubject", "flying");
            this.rankingGateway.deleteRanking(session, toDelete);
            tx.commit();
            tx = session.beginTransaction();
            Person subject = this.personGateway.getPeopleByName(session, "testSubject")
                    .stream()
                    .findFirst()
                    .orElse(new Person("testSubject"));
            Person observer = this.personGateway.getPeopleByName(session, "testObserver")
                    .stream()
                    .findFirst()
                    .orElse(new Person("testObserver"));
            this.personGateway.persistPersonWithUniqueName(session, subject);
            this.personGateway.persistPersonWithUniqueName(session, observer);
            Skill skill = this.skillGateway.getSkillByName(session, "flying").orElse(new Skill("flying"));
            this.skillGateway.saveSkill(session, skill);
            final Ranking ranking = new Ranking(subject, observer, skill, 44);
            this.rankingGateway.saveRanking(session, ranking);
            tx.commit();
            tx = session.beginTransaction();
            Ranking ranking1 = this.rankingGateway.getRankingBySubjectAndObserverAndSkill(session, "testSubject",
                    "testObserver", "flying").orElseThrow();
            Assertions.assertEquals(ranking, ranking1);
            tx.commit();
            tx = session.beginTransaction();
            Ranking r = this.rankingGateway
                    .getRankingBySubjectAndObserverAndSkill(session, "testSubject", "testObserver", "flying")
                    .orElseThrow();
            this.rankingGateway.deleteRanking(session, Collections.singletonList(r));
            tx.commit();
            tx = session.beginTransaction();
            Assertions.assertTrue(this.rankingGateway.getRankingBySubjectAndSkill(session, "testSubject", "flying").isEmpty());
            tx.commit();
        }
    }

    @Test
    public void shouldReturnAllRankingsForSubject() {
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            this.rankingGateway.deleteRanking(session, this.rankingGateway.getAllRankingsForSubject(session, "subject"));
            Person subject = this.personGateway.getPeopleByName(session, "subject").stream().findFirst().orElse(new Person());
            Person observer1 = this.personGateway.getPeopleByName(session, "observer1").stream().findFirst().orElse(new Person());
            Person observer2 = this.personGateway.getPeopleByName(session, "observer2").stream().findFirst().orElse(new Person());
            Skill skill = this.skillGateway.getSkillByName(session, "reading").orElse(new Skill("reading"));
            this.rankingGateway.persistRanking(session, new Ranking(subject, observer1, skill, 10));
            this.rankingGateway.persistRanking(session, new Ranking(subject, observer2, skill, 22));
            tx.commit();
            tx = session.beginTransaction();
            Assertions.assertEquals(2, this.rankingGateway.getAllRankingsForSubject(session, "subject").size());
            this.rankingGateway.deleteRanking(session, this.rankingGateway.getAllRankingsForSubject(session, "subject"));
            tx.commit();
        }
    }

    @Test
    public void testReturnMaxForSkill() {
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            this.rankingGateway.deleteRanking(session, this.rankingGateway.getAllRankingsForSubject(session, "subject"));
            Person subject = this.personGateway.getPeopleByName(session, "subject")
                    .stream()
                    .findFirst()
                    .orElseGet(() -> {
                        var p = new Person("subject");
                        this.personGateway.persistPersonWithUniqueName(session, p);
                        return p;
                    });
            Person observer1 = this.personGateway.getPeopleByName(session, "observer1")
                    .stream()
                    .findFirst()
                    .orElseGet(() -> {
                        var p = new Person("observer1");
                        this.personGateway.persistPersonWithUniqueName(session, p);
                        return p;
                    });
            Person observer2 = this.personGateway.getPeopleByName(session, "observer2")
                    .stream()
                    .findFirst()
                    .orElseGet(() -> {
                        var p = new Person("observer2");
                        this.personGateway.persistPersonWithUniqueName(session, p);
                        return p;
                    });
            Skill skill = this.skillGateway.getSkillByName(session, "reading").orElse(new Skill("reading"));
            this.rankingGateway.persistRanking(session, new Ranking(subject, observer1, skill, 10));
            this.rankingGateway.persistRanking(session, new Ranking(subject, observer2, skill, 22));
            this.rankingGateway.persistRanking(session, new Ranking(subject, observer2, skill, 15));
            tx.commit();
            tx = session.beginTransaction();
            Assertions.assertEquals(22, this.rankingGateway.findBestForGivenSkill(session, "reading"));
            this.rankingGateway.deleteRanking(session, this.rankingGateway.getAllRankingsForSubject(session, "subject"));
            tx.commit();
        }
    }

    @Test
    public void testReturnMaxFoNonExistingRanking() {
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            this.rankingGateway.deleteRanking(session, this.rankingGateway.getAllRankingsForSubject(session, "reading"));
            Assertions.assertEquals(-1, this.rankingGateway.findBestForGivenSkill(session, "reading"));
            tx.commit();
        }
    }

}
