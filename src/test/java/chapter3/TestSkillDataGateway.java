package chapter3;

import app.entities.Skill;
import app.gateways.SkillDataGateway;
import global.HibernateTest;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestSkillDataGateway extends HibernateTest {

    private static SessionFactory sessionFactory;
    private SkillDataGateway gateway;

    @BeforeAll
    public static void setUpSessionFactory() {
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure()
                .build();
        sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
    }

    @BeforeEach
    public void setUpGateway() {
        this.gateway = new SkillDataGateway();
    }

    @Test
    public void shouldSaveSkillToDatabase() {
        final Skill skill = new Skill("reading");
        try (final Session session = sessionFactory.openSession()) {
            final Long id = this.gateway.saveSkill(session, skill);
            this.gateway.getSkillByName(session, "reading").ifPresentOrElse(s -> {
                Assertions.assertEquals(id, s.getId());
                Assertions.assertEquals("reading", s.getName());
            }, Assertions::fail);
        }
    }

    @Test
    public void shouldPersistSkillToDataBase() {
        final Skill skill = new Skill("writing");
        try (final Session session = sessionFactory.openSession()) {
            final Transaction tx = session.beginTransaction();
            this.gateway.persistSkill(session, skill);
            tx.commit();
            Assertions.assertTrue(this.gateway.getSkillByName(session, "writing").isPresent());
        }
    }
}
