package chapter3;

import app.entities.Person;
import app.gateways.PersonDataGateway;
import global.HibernateTest;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestPersonDataGateway extends HibernateTest {
    private PersonDataGateway gateway;

    @BeforeEach
    public void setUpGateway() {
        this.gateway = new PersonDataGateway();
    }

    @Test
    public void personDataGatewayShouldSavePerson() {
        try (final Session session = sessionFactory.openSession()) {
            final Person person = new Person("subject");
            final Long id = this.gateway.savePersonWithUniqueName(session, person);
            this.gateway.getPeopleByName(session, "subject")
                    .stream()
                    .findFirst()
                    .ifPresentOrElse(p -> {
                        Assertions.assertEquals("subject", p.getName());
                        Assertions.assertEquals(id, p.getId());
                    }, Assertions::fail);
        }
    }

    @Test
    public void personDataGatewayShouldPersistPerson() {
        try (Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            Person person = new Person("persist");
            this.gateway.persistPersonWithUniqueName(session, person);
            tx.commit();
            this.gateway.getPeopleByName(session, "persist")
                    .stream()
                    .findFirst()
                    .ifPresentOrElse(p -> Assertions.assertEquals("persist", p.getName()), Assertions::fail);
        }
    }
}
