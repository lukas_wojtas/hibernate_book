package chapter5;

import app.entities.User;
import global.HibernateTest;
import org.hibernate.NonUniqueObjectException;
import org.hibernate.NonUniqueResultException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TableWithNoPrimaryKeyUsingCompositeKey extends HibernateTest {

    @AfterEach
    @BeforeEach
    public void tearDown() {
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            session.createQuery("from User", User.class).list().forEach(session::delete);
            tx.commit();
        }
    }

    @Test
    public void testSavingWithNoPrimaryKey() {
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            User user1 = new User("Name", 33, "Surname");
            User user2 = new User("Name1", 33, "Surname");
            User user3 = new User("Name", 32, "Surname");
            session.save(user1);
            session.save(user2);
            session.save(user3);
            tx.commit();
        }
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            Query<User> query1 = session.createQuery("from User u where u.name=:name and u.age=:age", User.class);
            query1.setParameter("name", "Name");
            query1.setParameter("age", 33);
            User user = query1.uniqueResult();
            Assertions.assertNotNull(user);
            Query<User> query2 = session.createQuery("from User", User.class);
            Assertions.assertEquals(3, query2.list().size());
            Query<User> query3 = session.createQuery("from User u where u.name=:name", User.class);
            query3.setParameter("name", "Name");
            Assertions.assertEquals(2, query3.list().size());
            Assertions.assertThrows(NonUniqueResultException.class, query3::uniqueResult);
            tx.commit();
        }
    }

    @Test
    public void testSavingWithConflict() {
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            User user1 = new User("Name", 33, "Surname");
            User user2 = new User("Name", 33, "Surname");
            session.save(user1);
            Assertions.assertThrows(NonUniqueObjectException.class, () -> session.save(user2));
            tx.commit();
        }
    }
}
