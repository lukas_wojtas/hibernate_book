package chapter9;

import app.entities.Supplier;
import global.HibernateTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * !IMPORTANT
 * Adding comments to hql queries supported only in hibernate
 * */
public class TestAddCommentToQuery extends HibernateTest {
    @BeforeEach
    @AfterEach
    public void cleanUp() {
        final var session = sessionFactory.openSession();
        this.execute(session, () -> session
                .createQuery("from Supplier", Supplier.class)
                .getResultList()
                .forEach(session::remove));
    }

    @Test
    public void addCommentToSqlQuery() {
        final var entityManager = sessionFactory.openSession();
        this.execute(entityManager, () -> {
            final var query = entityManager.createQuery("from Supplier", Supplier.class);
            query.setComment("comment for query");
            Assertions.assertTrue(query.list().isEmpty());
            Assertions.assertEquals("comment for query", query.getComment());
        });
    }
}
