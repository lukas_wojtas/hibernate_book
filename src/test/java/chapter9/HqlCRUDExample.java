package chapter9;

import app.entities.Person;
import global.JPATest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class HqlCRUDExample extends JPATest {

    @BeforeEach
    @AfterEach
    void cleanUp() {
        final var entityManager = JPATest.getEntityManager();
        this.execute(entityManager, () -> entityManager.createQuery("from Person", Person.class)
                .getResultList().forEach(entityManager::remove));
    }

    @Test
    void testHqlUpdate() {
        final var createEntityManager = JPATest.getEntityManager();
        List<Long> ids = new ArrayList<>();
        this.execute(createEntityManager, () -> {
            var person = new Person("name");
            createEntityManager.persist(person);
            ids.add(person.getId());
        });
        final var entEntityManager = JPATest.getEntityManager();
        this.execute(entEntityManager, () -> {
            int numberIfModifiedEntities = entEntityManager.createQuery("update Person p set name=:name where id=:id")
                    .setParameter("name", "name")
                    .setParameter("id", ids.get(0))
                    .executeUpdate();
            Assertions.assertEquals(1, numberIfModifiedEntities);
        });
    }

    @Test
    void testHqlDelete() {
        final var createEntityManager = JPATest.getEntityManager();
        this.execute(createEntityManager, () -> {
            final var person = new Person("name");
            createEntityManager.persist(person);
        });
        final var deleteEntityManager = JPATest.getEntityManager();
        this.execute(deleteEntityManager, () -> {
            var numberOfDeleted = deleteEntityManager.createQuery("delete Person p where name=:name")
                    .setParameter("name", "name")
                    .executeUpdate();
            Assertions.assertEquals(1, numberOfDeleted);
        });
        final var getEntityManager = JPATest.getEntityManager();
        this.execute(getEntityManager, () -> Assertions.assertEquals(0, getEntityManager
                .createQuery("from Person", Person.class)
                .getResultList()
                .size()));
    }

    @Test
    void testHqlInsert() {
        final var createEntityManager = JPATest.getEntityManager();
        this.execute(createEntityManager, () -> {
            Assertions.assertEquals(0, createEntityManager
                    .createQuery("from Person p where p.name=:name")
                    .setParameter("name", "name")
                    .getResultList().size());
            final var person = new Person("name");
            createEntityManager.persist(person);
        });
        final var insertEntityManager = JPATest.getEntityManager();
        this.execute(insertEntityManager, () -> {
            insertEntityManager.createQuery("insert into Person(name) " +
                    "select name from Person where name=:name")
                    .setParameter("name", "name")
                    .executeUpdate();
            Assertions.assertEquals(2, insertEntityManager.createQuery("from Person p where p.name=:name")
                    .setParameter("name", "name").getResultList().size());
        });
    }

    @Test
    void testHqlSelect() {
        final var createEntityManager = JPATest.getEntityManager();
        this.execute(createEntityManager, () -> {
            Assertions.assertEquals(0, createEntityManager.createQuery("from Person p where p.name=:name")
                    .setParameter("name", "name")
                    .getResultList().size());
            createEntityManager.persist(new Person("name"));

        });
        final var selectEntityManger = JPATest.getEntityManager();
        this.execute(selectEntityManger,
                () -> Assertions.assertNotNull(selectEntityManger
                        .createQuery("from Person p where p.name=:name", Person.class)
                        .setParameter("name", "name").getSingleResult()));
    }
}
