package chapter9;

import app.entities.Product;
import app.entities.Software;
import app.entities.Supplier;
import global.JPATest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TestNamedQueries extends JPATest {

    private static final String SUPPLIER_NAME = "supplierName";

    @BeforeEach
    @AfterEach
    public void cleanUp() {
        final var entityManager = JPATest.getEntityManager();
        this.execute(entityManager, () -> entityManager
                .createQuery("from Supplier", Supplier.class)
                .getResultList()
                .forEach(entityManager::remove));
    }

    @Test
    public void sanityTestIfEverythingIsWorkingAsDesigned() {
        final var entityManager = JPATest.getEntityManager();
        this.execute(entityManager, () -> {
            final var supplier = Supplier.builder().name(SUPPLIER_NAME).build();
            final var products = IntStream.range(0, 3).mapToObj(index ->
                    Product.builder()
                            .name("product_" + index)
                            .description("description_" + index)
                            .price(Double.valueOf(index + ""))
                            .supplier(supplier)
                            .build()
            ).collect(Collectors.toList());
            var softwareProduct = Software
                    .builder()
                    .description("software")
                    .name("software")
                    .price(5.5)
                    .version("version")
                    .supplier(supplier)
                    .build();
            entityManager.persist(supplier);
            supplier.setProducts(products);
            products.forEach(entityManager::persist);
            entityManager.persist(softwareProduct);
        });
        final var checkEntityManager = JPATest.getEntityManager();
        this.execute(checkEntityManager, () -> {
            var products = checkEntityManager.createQuery("from Product", Product.class).getResultList();
            var supplier = checkEntityManager.createQuery("from Supplier", Supplier.class).getSingleResult();
            var software = checkEntityManager.createQuery("from Software", Software.class).getSingleResult();
            Assertions.assertEquals(4, products.size());
            products.forEach(p -> Assertions.assertEquals(SUPPLIER_NAME, p.getSupplier().getName()));
            Assertions.assertNotNull(supplier);
            Assertions.assertEquals(4, supplier.getProducts().size());
            Assertions.assertNotNull(software);
            Assertions.assertEquals(SUPPLIER_NAME, software.getSupplier().getName());
        });
    }

    @Test
    public void shouldUseNameQueryToGetSupplier() {
        final var entityManager = JPATest.getEntityManager();
        this.execute(entityManager, () -> entityManager.persist(Supplier.builder().name(SUPPLIER_NAME).build()));
        final var checkEntityManager = JPATest.getEntityManager();
        this.execute(checkEntityManager, () -> {
            var supplier = checkEntityManager
                    .createNamedQuery("supplier.findAll", Supplier.class).getSingleResult();
            Assertions.assertEquals(SUPPLIER_NAME, supplier.getName());
            supplier = checkEntityManager.createNamedQuery("supplier.findByName", Supplier.class)
                    .setParameter("name", SUPPLIER_NAME)
                    .getSingleResult();
            Assertions.assertEquals(SUPPLIER_NAME, supplier.getName());
        });
    }

    @Test
    public void shouldAddCommentToQuery() {
        final var entityManager = JPATest.getEntityManager();
        this.execute(entityManager, () -> entityManager.persist(Supplier.builder().name(SUPPLIER_NAME).build()));
        final var checkEntityManager = JPATest.getEntityManager();
        this.execute(checkEntityManager, () -> {
            var supplier = checkEntityManager.createQuery("from Supplier")
                    .setHint("org.hibernate.comment", ">>>>>>>>>>>>> This is my comment")
                    .getSingleResult();
            Assertions.assertNotNull(supplier);
        });
    }
}
