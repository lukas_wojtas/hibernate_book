package chapter9;

import app.entities.Product;
import app.entities.Supplier;
import global.JPATest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TestHqlFromAndSelect extends JPATest {

    @BeforeEach
    @AfterEach
    public void setUp() {
        final var entityManager = JPATest.getEntityManager();
        this.execute(entityManager, () -> entityManager
                .createQuery("from Supplier", Supplier.class)
                .getResultList()
                .forEach(entityManager::remove));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void shouldSelectResultSetWithNameOnly() {
        final var entityManager = JPATest.getEntityManager();
        this.execute(entityManager, () -> {
            final var supplier = Supplier.builder().name("biker").build();
            final var products = IntStream.range(0, 5).mapToObj(index ->
                    Product.builder()
                            .supplier(supplier)
                            .price((double) index)
                            .description("Product nr " + index)
                            .name("Name " + index).build())
                    .collect(Collectors.toList());
            products.forEach(product -> product.setSupplier(supplier));
            entityManager.persist(supplier);
            products.forEach(entityManager::persist);
        });
        final var readEntityManager = JPATest.getEntityManager();
        this.execute(readEntityManager, () -> {
            final var productNames = readEntityManager
                    .createQuery("select p.name from Product p", String.class)
                    .getResultList();
            Assertions.assertEquals(5, productNames.size());
            productNames.forEach(p -> Assertions.assertTrue(p.contains("Name")));
            final var productPrices = readEntityManager
                    .createQuery("select p.price from Product p", Double.class)
                    .getResultList();
            Assertions.assertEquals(5, productPrices.size());
            productPrices.forEach(price -> {
                Assertions.assertTrue(price >= 0);
                Assertions.assertTrue(price < 5);
            });
            final var singleProductList = readEntityManager
                    .createQuery("select p.name from Product p where p.price=:price", String.class)
                    .setParameter("price", 3.0)
                    .getResultList();
            Assertions.assertEquals(1, singleProductList.size());
            Assertions.assertEquals("Name 3", singleProductList.get(0));
            final var productNameAndPrice = readEntityManager
                    .createQuery("select p.name, p.description from Product p")
                    .getResultList();
            productNameAndPrice.forEach(p -> {
                Object[] array = (Object[]) p;
                Assertions.assertEquals(2, array.length);
            });
        });
    }
}
