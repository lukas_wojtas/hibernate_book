package chapter7;

import app.entities.HookEntity;
import app.entities.HookEntityExternalListener;
import app.entities.HookEntityWithExceptions;
import app.entities.HookEntityWithExternalHook;
import global.JPATest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import java.util.Optional;


@Slf4j
public class TestHooks extends JPATest {

    @BeforeEach
    @AfterEach
    public void cleanUp() {
        final var entityManager = JPATest.getEntityManager();
        this.execute(entityManager, () -> entityManager.createQuery("select h from HookEntity h", HookEntity.class)
                .getResultList().forEach(entityManager::remove));
    }

    @Test
    public void shouldCallPersistHooks() {
        final var createEntityManager = JPATest.getEntityManager();
        this.execute(createEntityManager, () -> {
            var hook = new HookEntity("hook");
            createEntityManager.persist(hook);
            Assertions.assertTrue(hook.getHooks().contains("beforePersist"));
            Assertions.assertTrue(hook.getHooks().contains("afterPersist"));
        });
    }

    @Test
    public void shouldCallUpdateHooks() {
        final var createEntityManager = JPATest.getEntityManager();
        this.execute(createEntityManager, () -> {
            var hook = new HookEntity("value");
            createEntityManager.persist(hook);
        });
        EntityManager updateEntityManager = null;
        try {
            updateEntityManager = JPATest.getEntityManager();
            updateEntityManager.getTransaction().begin();
            var hook = updateEntityManager.createQuery("select h from HookEntity h where h.value=:value", HookEntity.class)
                    .setParameter("value", "value").getSingleResult();
            hook.setValue("new Value");
            updateEntityManager.getTransaction().commit();
            Assertions.assertTrue(hook.getHooks().contains("beforeUpdate"));
            Assertions.assertTrue(hook.getHooks().contains("afterUpdate"));
        } catch (Exception e) {
            log.error(e.getLocalizedMessage());
            throw e;
        } finally {
            Optional.ofNullable(updateEntityManager).ifPresent(e -> {
                if (e.isOpen()) {
                    e.close();
                }
            });
        }
    }

    @Test
    public void shouldCallLoadHook() {
        final var createEntityManager = JPATest.getEntityManager();
        this.execute(createEntityManager, () -> {
            var hook = new HookEntity("value");
            createEntityManager.persist(hook);
        });
        final var loadEntityManager = JPATest.getEntityManager();
        this.execute(loadEntityManager, () -> {
            var hook = loadEntityManager.createQuery("select h from HookEntity h where h.value=:value", HookEntity.class)
                    .setParameter("value", "value").getSingleResult();
            Assertions.assertNotNull(hook);
            Assertions.assertTrue(hook.getHooks().contains("afterLoad"));
        });
    }

    @Test
    public void shouldCallRemoveHooks() {
        final var createEntityManager = JPATest.getEntityManager();
        this.execute(createEntityManager, () -> {
            var hook = new HookEntity("value");
            createEntityManager.persist(hook);
        });
        EntityManager deleteEntityManager = null;
        try {
            deleteEntityManager = JPATest.getEntityManager();
            deleteEntityManager.getTransaction().begin();
            var hook = deleteEntityManager.createQuery("select e from HookEntity e", HookEntity.class).getSingleResult();
            deleteEntityManager.remove(hook);
            deleteEntityManager.getTransaction().commit();
            Assertions.assertTrue(hook.getHooks().contains("beforeRemove"));
            Assertions.assertTrue(hook.getHooks().contains("afterRemove"));
        } catch (Exception e) {
            log.error(e.getLocalizedMessage());
            throw e;
        } finally {
            Optional.ofNullable(deleteEntityManager).ifPresent(e -> {
                if (e.isOpen()) {
                    e.close();
                }
            });
        }
    }

    @Test
    public void shouldThrowExceptionInBeforeHook() {
        EntityManager createEntityManager = null;
        try {
            createEntityManager = JPATest.getEntityManager();
            var tx = createEntityManager.getTransaction();
            tx.begin();
            Assertions.assertTrue(tx.isActive());
            try {
                createEntityManager.persist(new HookEntityWithExceptions("value", true, false));
            } catch (Exception e) {
                Assertions.assertEquals("before Hook", e.getLocalizedMessage());
            }
            Assertions.assertTrue(tx.isActive());
            tx.commit();
        } catch (Exception e) {
            log.error(e.getLocalizedMessage());
            throw e;
        } finally {
            Optional.ofNullable(createEntityManager).ifPresent(e -> {
                if (e.isOpen()) {
                    e.close();
                }
            });
        }
        var checkEntityManager = JPATest.getEntityManager();
        this.execute(checkEntityManager, () -> Assertions.assertEquals(0, checkEntityManager
                .createQuery("select h from HookEntityWithExceptions h", HookEntityWithExceptions.class).getResultList().size()));
    }

    @Test
    public void shouldThrowExceptionInAfterHook() {
        EntityManager createEntityManager = null;
        try {
            createEntityManager = JPATest.getEntityManager();
            var tx = createEntityManager.getTransaction();
            tx.begin();
            Assertions.assertTrue(tx.isActive());
            try {
                createEntityManager.persist(new HookEntityWithExceptions("value", false, true));
            } catch (Exception e) {
                Assertions.assertEquals("after Hook", e.getLocalizedMessage());
            }
            Assertions.assertTrue(tx.isActive());
            tx.commit();
        } catch (Exception e) {
            log.error(e.getLocalizedMessage());
            throw e;
        } finally {
            Optional.ofNullable(createEntityManager).ifPresent(e -> {
                if (e.isOpen()) {
                    e.close();
                }
            });
        }
        var checkEntityManager = JPATest.getEntityManager();
        this.execute(checkEntityManager, () -> Assertions.assertEquals(0, checkEntityManager
                .createQuery("select h from HookEntityWithExceptions h", HookEntityWithExceptions.class).getResultList().size()));
    }

    @Test
    public void shouldCallExternalListener() {
        EntityManager createEntityManager = null;
        try {
            createEntityManager = JPATest.getEntityManager();
            createEntityManager.getTransaction().begin();
            createEntityManager.persist(new HookEntityWithExternalHook("value"));
            createEntityManager.getTransaction().commit();
            Assertions.assertTrue(HookEntityExternalListener.getEvents().contains("beforePersist"));
        } catch (Exception e) {
            log.error(e.getLocalizedMessage());
            throw e;
        } finally {
            Optional.ofNullable(createEntityManager).ifPresent(e -> {
                if (e.isOpen()) {
                    e.close();
                }
            });
        }

    }
}
