package chapter7;

import app.entities.Person;
import global.JPATest;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

@Slf4j
public class TestSimpleInsertionWithJPA extends JPATest {

    @BeforeEach
    @AfterEach
    public void cleanUp() {
        final var entityManager = JPATest.getEntityManager();
        this.execute(entityManager, () -> entityManager.createQuery("select p from Person p", Person.class)
                .getResultList().forEach(entityManager::remove));
    }

    @Test
    public void shouldCreatePersonWithPureJpa() {
        final var entityManager = JPATest.getEntityManager();
        this.execute(entityManager, () -> entityManager.persist(new Person("name")));
        final var entityManagerToCheck = JPATest.getEntityManager();
        this.execute(entityManagerToCheck, () -> {
            var person = entityManagerToCheck.createQuery("select p from Person p", Person.class).getSingleResult();
            Assertions.assertNotNull(person);
            Assertions.assertEquals("name", person.getName());
        });
    }

    @Test
    public void shouldCreatePersonWithHibernateSessionWrappedFromJpaEntityManager() {
        try(final Session session = JPATest.getSession()) {
            Transaction tx = session.beginTransaction();
            session.save(new Person("name"));
            tx.commit();
        }
        try(final Session session = JPATest.getSession()) {
            Transaction tx = session.beginTransaction();
            var person = session.createQuery("from Person", Person.class).uniqueResult();
            tx.commit();
            Assertions.assertNotNull(person);
            Assertions.assertEquals("name", person.getName());
        }
    }
}
