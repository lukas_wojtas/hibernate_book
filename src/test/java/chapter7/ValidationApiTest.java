package chapter7;

import app.entities.CoordinatesWithCustomValidation;
import app.entities.EmailWithValidation;
import app.entities.MessageWithValidation;
import global.JPATest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolationException;

@Slf4j
public class ValidationApiTest extends JPATest {

    @BeforeEach
    @AfterEach
    public void setUp() {
        final var entityManager = JPATest.getEntityManager();
        this.execute(entityManager, () -> {
            entityManager.createQuery("select e from EmailWithValidation e",
                    EmailWithValidation.class).getResultList().forEach(entityManager::remove);
            entityManager.createQuery("select m from MessageWithValidation m",
                    MessageWithValidation.class).getResultList().forEach(entityManager::remove);
            entityManager.createQuery("select c from CoordinatesWithCustomValidation c",
                    CoordinatesWithCustomValidation.class).getResultList().forEach(entityManager::remove);
        });
    }

    @Test
    public void shouldCreateEntitiesWithConstraints() {
        final var entityManager = JPATest.getEntityManager();
        final var mail = new EmailWithValidation("address@domain.com");
        final var message = new MessageWithValidation("text", "message", 15);
        mail.setMessageInEmail(message);
        message.setEmail(mail);
        this.execute(entityManager, () -> entityManager.persist(mail));
        Assertions.assertNotNull(mail.getId());
        final var getEntityManager = JPATest.getEntityManager();
        this.execute(getEntityManager, () -> {
            var email = getEntityManager.find(EmailWithValidation.class, mail.getId());
            var msg = email.getMessageInEmail();
            Assertions.assertEquals("message", msg.getMessage());
            Assertions.assertEquals("text", msg.getText());
            Assertions.assertEquals("address@domain.com", email.getEmail());
            Assertions.assertEquals(15, message.getTestNumber());
        });
    }

    @Test
    public void shouldFailToCreateMessageBecauseTextToShort() {
        final var entityManager = JPATest.getEntityManager();
        final var message = new MessageWithValidation("", "message", 15);
        try {
            this.execute(entityManager, () -> entityManager.persist(message));
            Assertions.fail();
        } catch (ConstraintViolationException e) {
            final var violations = e.getConstraintViolations();
            Assertions.assertEquals(1, violations.size());
            violations.forEach(v -> Assertions.assertEquals("text", v.getPropertyPath().toString()));
        }
    }

    @Test
    public void shouldFailToCreateMessageBecauseTextToLong() {
        final var entityManager = JPATest.getEntityManager();
        final var message = new MessageWithValidation("fantastic and very long text",
                "message", 15);
        Assertions.assertThrows(ConstraintViolationException.class, () -> this.execute(entityManager,
                () -> entityManager.persist(message)));
    }

    @Test
    public void testConstraintsOnTestNumber() {
        final var entityManager = JPATest.getEntityManager();
        Assertions.assertThrows(ConstraintViolationException.class, () -> this.execute(entityManager,
                () -> entityManager.persist(new MessageWithValidation("text", "message", 9))));
        final var correct = JPATest.getEntityManager();
        this.execute(correct, () -> {
            correct.persist(new MessageWithValidation("text", "message", 10));
            correct.persist(new MessageWithValidation("text", "message", 20));
        });
        final var wrong = JPATest.getEntityManager();
        Assertions.assertThrows(ConstraintViolationException.class, () -> this.execute(wrong,
                () -> wrong.persist(new MessageWithValidation("text", "message", 21))));
    }

    @Test
    public void shouldNotAllowMessageToBeNull() {
        final var manager = JPATest.getEntityManager();
        Assertions.assertThrows(ConstraintViolationException.class,
                () -> this.execute(manager, () -> manager.persist(new MessageWithValidation("text", 15))));
        final var manager1 = JPATest.getEntityManager();
        Assertions.assertThrows(ConstraintViolationException.class,
                () -> this.execute(manager1, () -> manager1.persist(new MessageWithValidation("text", "", 15))));
        final var manager2 = JPATest.getEntityManager();
        Assertions.assertThrows(ConstraintViolationException.class,
                () -> this.execute(manager2, () -> manager2.persist(new MessageWithValidation("text", " ", 15))));
    }

    @Test
    public void shouldUseCustomConstraint() {
        final var manager = JPATest.getEntityManager();
        Assertions.assertThrows(ConstraintViolationException.class,
                () -> this.execute(manager, () -> manager.persist(new CoordinatesWithCustomValidation(-1, -1))));
        final var manager1 = JPATest.getEntityManager();
        this.execute(manager1, () -> manager1.persist(new CoordinatesWithCustomValidation(1, 1)));
        final var manager2 = JPATest.getEntityManager();
        this.execute(manager2, () -> Assertions.assertNotNull(manager2.createQuery("select c from CoordinatesWithCustomValidation c",
                CoordinatesWithCustomValidation.class).getSingleResult()));
    }
}
