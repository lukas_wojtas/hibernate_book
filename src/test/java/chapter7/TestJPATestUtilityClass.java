package chapter7;

import global.JPATest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.persistence.PersistenceException;

public class TestJPATestUtilityClass extends JPATest{

    private static final String NON_EXISTENT_SESSION_NAME = "not_exists";

    @Test
    public void shouldInitializeEntityManager() {
        Assertions.assertNotNull(JPATest.getEntityManager());
    }

    @Test
    public void shouldFailToInitializeManager() {
        Assertions.assertThrows(PersistenceException.class,
                () -> JPATest.getEntityManager(NON_EXISTENT_SESSION_NAME));
    }

    @Test
    public void shouldInitializeHibernateSession() {
        Assertions.assertNotNull(JPATest.getSession());
    }

    @Test
    public void shouldFailToInitializeHibernateSession() {
        Assertions.assertThrows(PersistenceException.class, () -> JPATest.getSession(NON_EXISTENT_SESSION_NAME));
    }
}
