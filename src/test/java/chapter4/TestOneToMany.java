package chapter4;

import app.entities.Book;
import app.entities.Library;
import global.HibernateTest;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class TestOneToMany extends HibernateTest {

    @AfterEach
    @BeforeEach
    public void tearDown() {
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            //Not needed because of orphan removal
            //session.createQuery("from Book", Book.class).list().forEach(session::delete);
            session.createQuery("from Library", Library.class).list().forEach(session::delete);
            tx.commit();
        }
    }

    @Test
    public void basicLibraryWithBooksTest() {
        Long libraryId;
        Library library;
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            library = new Library("Baczus Library");
            //Save library first, no cascade set
            session.save(library);
            Assertions.assertNotNull(library.getId());
            //Make books
            Book b1 = new Book("cat");
            Book b2 = new Book("pig");
            List<Book> books = new ArrayList<>();
            books.add(b1);
            books.add(b2);
            library.setBooks(books);
            b1.setLibrary(library);
            b2.setLibrary(library);
            session.save(b1);
            session.save(b2);
            libraryId = library.getId();
            tx.commit();
        }
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            library = session.get(Library.class, libraryId);
            Assertions.assertNotNull(library);
            Assertions.assertEquals(2, library.getBooks().size());
            tx.commit();
        }
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            Assertions.assertNotNull(library);
            session.remove(library);
            Assertions.assertEquals(0, session.createQuery("from Book", Book.class).list().size());
            tx.commit();
        }
    }
}
