package chapter4;

import app.entities.Email;
import app.entities.Message;
import global.HibernateTest;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CascadingExamples extends HibernateTest {

    @AfterEach
    public void tearDown() {
        try(final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            session.createQuery("from Message", Message.class).list().forEach(session::delete);
            tx.commit();
        }
    }

    @Test
    public void emailToMessageCascadeOnPersistAndDelete() {
        Long emailId;
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            Message message = new Message("text","message");
            Email email = new Email("email");
            message.setEmail(email);
            email.setMessage(message);
            session.persist(message);
            emailId = message.getEmail().getId();
            Assertions.assertNotNull(emailId);
            tx.commit();
        }
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            Email email = session.get(Email.class, emailId);
            Assertions.assertNotNull(email);
            tx.commit();
        }
    }

}
