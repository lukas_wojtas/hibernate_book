package chapter4;

import app.entities.Email;
import app.entities.Message;
import global.HibernateTest;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestBiDirectionalOneToOne extends HibernateTest {

    @BeforeEach
    @AfterEach
    public void cleanUp() {
        try (final var session = sessionFactory.openSession()) {
            this.execute(session, () -> session.createQuery("from Email", Email.class).list().forEach(session::delete));
        }
    }

    @Test
    public void testEmailWithMessage() {
        Email email;
        Message message;
        Transaction tx;
        long emailId;
        long messageId;
        try (final Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            email = new Email("email");
            message = new Message("text","message");
            email.setMessage(message);
            message.setEmail(email);
            session.save(email);
            session.save(message);
            emailId = email.getId();
            messageId = message.getId();
            tx.commit();
        }
        try (final Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            Email e = session.get(Email.class, emailId);
            Message m = session.get(Message.class, messageId);
            Assertions.assertEquals("text", e.getMessage().getText());
            Assertions.assertEquals("message", e.getMessage().getMessage());
            Assertions.assertEquals("email", m.getEmail().getEmail());
            tx.commit();
            tx = session.beginTransaction();
            session.delete(e);
            session.remove(m);
            tx.commit();
        }
    }

    @Test
    public void testEmailWithMessageNotInitializedProperlyAndWorks() {
        Email email;
        Message message;
        Transaction tx;
        long emailId;
        long messageId;
        try (final Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            email = new Email("email");
            message = new Message("text", "message");
            email.setMessage(message);
            //message.setEmail(email); this is not set up
            session.save(email);
            session.save(message);
            emailId = email.getId();
            messageId = message.getId();
            tx.commit();
        }
        Assertions.assertNull(message.getEmail());
        Assertions.assertNotNull(email.getMessage());
        try (final Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            email = session.get(Email.class, emailId);
            message = session.get(Message.class, messageId);
            tx.commit();
        }
        Assertions.assertNotNull(message.getEmail());//message has email set
        Assertions.assertNotNull(email.getMessage());
        try (final Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            session.delete(email);
            session.remove(message);
            tx.commit();
        }
    }
}
