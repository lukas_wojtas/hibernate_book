package chapter4;

import app.entities.SimpleObject;
import global.HibernateTest;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MergeVsRefreshExample extends HibernateTest {

    @AfterEach
    public void tearDown() {
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            session.createQuery("from SimpleObject", SimpleObject.class)
                    .list()
                    .forEach(session::delete);
            tx.commit();
        }
    }

    @Test
    public void showHowMergeWorks() {
        //create object
        SimpleObject obj;
        Integer id;
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            obj = new SimpleObject("example", 3);
            Assertions.assertNull(obj.getId());
            session.save(obj);
            id = obj.getId();
            tx.commit();
        }
        //detach object
        Assertions.assertEquals(3, obj.getValue());
        obj.setValue(55);
        validateSimpleObject(id, 3);
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            //merge object
            session.merge(obj);
            tx.commit();
        }
        validateSimpleObject(id, 55);
    }

    @Test
    public void showHowRefreshWorks() {
        //create object
        SimpleObject obj;
        Integer id;
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            obj = new SimpleObject("example", 3);
            Assertions.assertNull(obj.getId());
            session.save(obj);
            id = obj.getId();
            tx.commit();
        }
        //detach object
        Assertions.assertEquals(3, obj.getValue());
        obj.setValue(55);
        validateSimpleObject(id, 3);
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            //refresh object
            session.refresh(obj);
            tx.commit();
        }
        validateSimpleObject(id, 3);
    }

    private void validateSimpleObject(Integer id, Integer value) {
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            SimpleObject toValidate = session.get(SimpleObject.class, id);
            Assertions.assertNotNull(toValidate);
            Assertions.assertEquals(value, toValidate.getValue());
            tx.commit();
        }
    }
}
