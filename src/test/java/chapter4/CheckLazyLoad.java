package chapter4;

import app.entities.Book;
import app.entities.Library;
import global.HibernateTest;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Collections;

public class CheckLazyLoad extends HibernateTest {

    @AfterEach
    public void tearDown() {
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            session.createQuery("from Library", Library.class).list().forEach(session::delete);
            tx.commit();
        }
    }

    @Test
    public void checkLazyLoading() {
        Long libraryId;
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            Library library = new Library("library");
            session.save(library);
            libraryId = library.getId();
            Book book = new Book("book");
            library.setBooks(Collections.singletonList(book));
            book.setLibrary(library);
            session.save(book);
            tx.commit();
        }

        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            Library library = session.get(Library.class, libraryId);
            //Collection is not loaded here
            Assertions.assertFalse(Hibernate.isInitialized(library.getBooks()));
            //Here Hibernate loading collection
            Assertions.assertEquals(1, library.getBooks().size());
            //Check that collection is loaded here
            Assertions.assertTrue(Hibernate.isInitialized(library.getBooks()));
            tx.commit();
        }
    }

}
