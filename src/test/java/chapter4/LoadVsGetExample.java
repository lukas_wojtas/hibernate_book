package chapter4;

import app.entities.SimpleObject;
import global.HibernateTest;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LoadVsGetExample extends HibernateTest {

    @AfterEach
    public void tearDown() {
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            Query<SimpleObject> query = session.createQuery("from SimpleObject", SimpleObject.class);
            query.list().forEach(session::delete);
            tx.commit();
        }
    }

    @Test
    public void loadNonExistingObject() {
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            //Exception thrown when accessing object
            Assertions.assertThrows(ObjectNotFoundException.class, () -> {
                SimpleObject load = session.load(SimpleObject.class, 33);
                Assertions.assertNull(load);
            });
            tx.commit();
        }

        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            SimpleObject get = session.get(SimpleObject.class, 33);
            Assertions.assertNull(get);
            tx.commit();
        }
    }
}
