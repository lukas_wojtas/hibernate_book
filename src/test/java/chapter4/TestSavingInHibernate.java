package chapter4;

import app.entities.SimpleObject;
import global.HibernateTest;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Random;

public class TestSavingInHibernate extends HibernateTest {

    @AfterEach
    public void tearDown() {
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            Query<SimpleObject> query = session.createQuery("from SimpleObject", SimpleObject.class);
            query.list().forEach(session::delete);
            tx.commit();
        }
    }

    @Test
    public void saveSimpleObject() {
        SimpleObject simpleObject;
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            simpleObject = new SimpleObject("key", 33);
            Assertions.assertNull(simpleObject.getId());
            session.save(simpleObject);
            Assertions.assertNotNull(simpleObject.getId());
            tx.commit();
        }
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            session.delete(simpleObject);
            tx.commit();
        }
    }

    @Test
    public void saveObjectThatAlreadyWasPersisted() {
        SimpleObject simpleObject;
        Integer id;
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            simpleObject = new SimpleObject("key", 33);
            Assertions.assertNull(simpleObject.getId());
            session.save(simpleObject);
            Assertions.assertNotNull(simpleObject.getId());
            id = simpleObject.getId();
            tx.commit();
        }
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            int secId = new Random().nextInt();
            while (secId == id) {
                secId = new Random().nextInt();
            }
            Assertions.assertNotEquals(id, secId);
            session.save(simpleObject);
            Query<SimpleObject> query = session.createQuery("from SimpleObject", SimpleObject.class);
            Assertions.assertEquals(2, query.list().size());
            tx.commit();
        }
    }

    @Test
    public void testSaveOrUpdate() {
        SimpleObject simpleObject;
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            simpleObject = new SimpleObject("mark", 5);
            //to here object in Transient state, from here state is persistent
            session.save(simpleObject);
            tx.commit();
        }
        //here simple object is in transient state because session was closed
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            Assertions.assertEquals("mark", simpleObject.getKey());
            Assertions.assertEquals(5, simpleObject.getValue());
            simpleObject.setKey("newMark");
            simpleObject.setValue(33);
            //From this line object is in persistent state
            session.saveOrUpdate(simpleObject);
            tx.commit();
        }
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            Query<SimpleObject> query = session.createQuery("from SimpleObject", SimpleObject.class);
            Assertions.assertEquals(1, query.list().size());
            Assertions.assertEquals("newMark", simpleObject.getKey());
            Assertions.assertEquals(33, simpleObject.getValue());
            tx.commit();
        }
    }

    @Test
    public void testEqualityOfSingleEntityFromSameSessionAndDifferentSessions() {
        Integer id;
        SimpleObject simpleObject;
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            simpleObject = new SimpleObject("mark", 5);
            Assertions.assertNull(simpleObject.getId());
            session.save(simpleObject);
            Assertions.assertNotNull(simpleObject.getId());
            id = simpleObject.getId();
            tx.commit();
        }
        // Check equality of object in the same session
        try (final Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            SimpleObject o1 = session.get(SimpleObject.class, id);
            SimpleObject o2 = session.get(SimpleObject.class, id);
            Assertions.assertEquals(o2, o1);
            Assertions.assertSame(o1, o2);
            //Check equality of object load previously in different session
            Assertions.assertEquals(simpleObject, o1);
            Assertions.assertEquals(simpleObject, o2);
            Assertions.assertNotSame(simpleObject, o1);
            Assertions.assertNotSame(simpleObject, o2);
            tx.commit();
        }
    }
}
