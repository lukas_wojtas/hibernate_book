/*
 * Pure java jdbc database handling
 * */
package chapter1;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TestPureJDBC {

    @Test
    @Disabled
    public void testPureJDBC() {
        final String SELECT = "SELECT id, text FROM messages";
        final String JDBCURL = "jdbc:mysql://localhost:3306/book";
        try (Connection connection = DriverManager.getConnection(JDBCURL, "lukasz", "meteor1981")) {
            try (PreparedStatement ps = connection.prepareStatement(SELECT)) {
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        System.out.println(rs.getString("text"));
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
