package global;

import javax.persistence.EntityManager;

public interface AutoClosableEntityManager extends EntityManager, AutoCloseable {

    @Override
    default void close() {
        try {
            this.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
