package global;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public abstract class JPATest {

    private static final String DEFAULT_PERSISTENCE_CONFIG_NAME = "hibernate_book";

    private static final Map<String, EntityManagerFactory> persistenceUnits = new HashMap<>();

    /*
    * Getting entity manager straight from JPA configuration
    * */
    public static synchronized EntityManager getEntityManager(String persistenceUnitName) {
        persistenceUnits.putIfAbsent(persistenceUnitName,
                Persistence.createEntityManagerFactory(persistenceUnitName));
        return persistenceUnits.get(persistenceUnitName)
                .createEntityManager();
    }

    public static synchronized EntityManager getEntityManager() {
        persistenceUnits.putIfAbsent(DEFAULT_PERSISTENCE_CONFIG_NAME,
                Persistence.createEntityManagerFactory(DEFAULT_PERSISTENCE_CONFIG_NAME));
        return persistenceUnits.get(DEFAULT_PERSISTENCE_CONFIG_NAME)
                .createEntityManager();
    }
    /*
    * Changing JPA EntityManager to Hibernate session.
    * */
    public static Session getSession(String persistenceUnitName) {
        return getEntityManager(persistenceUnitName).unwrap(Session.class);
    }

    public static Session getSession() {
        return getEntityManager(DEFAULT_PERSISTENCE_CONFIG_NAME).unwrap(Session.class);
    }

    public void execute(EntityManager entityManager, Runnable runnable) {
        try {
            entityManager.getTransaction().begin();
            runnable.run();
            entityManager.getTransaction().commit();
        }catch (Exception e) {
            log.error(e.getLocalizedMessage());
            throw e;
        } finally {
            if(entityManager.isOpen()) {
                entityManager.close();
            }
        }
    }

}
