package global;

import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.io.File;
import java.net.URISyntaxException;
import java.util.Optional;

public class HibernateTestTransactionLevel {

    public static SessionFactory configureCustomSessionFactoryWithIsolationLevelSerializable() throws URISyntaxException {
        var resource = Optional.ofNullable(HibernateTestTransactionLevel.class.getClassLoader()
                .getResource("hibernate.cfg.serializable.xml"))
                .orElseThrow(() -> new IllegalArgumentException("Hibernate configuration file not found"));
        var registry = new StandardServiceRegistryBuilder()
                .configure(new File(resource.toURI()))
                .build();
        return new MetadataSources(registry).buildMetadata().buildSessionFactory();
    }

    public static SessionFactory configureCustomSessionFactoryWithIsolationLevelRepeatableReads() throws URISyntaxException {
        var resource = Optional.ofNullable(HibernateTestTransactionLevel.class.getClassLoader()
                .getResource("hibernate.cfg.repeatable.reads.xml"))
                .orElseThrow(() -> new IllegalArgumentException("Hibernate configuration file not found"));
        var registry = new StandardServiceRegistryBuilder()
                .configure(new File(resource.toURI()))
                .build();
        return new MetadataSources(registry).buildMetadata().buildSessionFactory();
    }

    public static SessionFactory configureCustomSessionFactoryWithIsolationLevelReadCommitted() throws URISyntaxException {
        var resource = Optional.ofNullable(HibernateTestTransactionLevel.class.getClassLoader()
                .getResource("hibernate.cfg.read.committed.cfg.xml"))
                .orElseThrow(() -> new IllegalArgumentException("Hibernate configuration file not found"));
        var registry = new StandardServiceRegistryBuilder()
                .configure(new File(resource.toURI()))
                .build();
        return new MetadataSources(registry).buildMetadata().buildSessionFactory();
    }

    public static SessionFactory configureCustomSessionFactoryWithIsolationLevelReadUncommitted() throws URISyntaxException {
        var resource = Optional.ofNullable(HibernateTestTransactionLevel.class.getClassLoader()
                .getResource("hibernate.cfg.read.uncommitted.xml"))
                .orElseThrow(() -> new IllegalArgumentException("Hibernate configuration file not found"));
        var registry = new StandardServiceRegistryBuilder()
                .configure(new File(resource.toURI()))
                .build();
        return new MetadataSources(registry).buildMetadata().buildSessionFactory();
    }
}
