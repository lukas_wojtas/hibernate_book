package global;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.junit.jupiter.api.BeforeAll;

public abstract class HibernateTest {
    protected static SessionFactory sessionFactory;

    @BeforeAll
    public static void setUpSessionFactory() {
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure()
                .build();
        sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
    }

    public void execute(Session s, Runnable r) {
        Transaction tx = s.beginTransaction();
        r.run();
        tx.commit();
    }
}
