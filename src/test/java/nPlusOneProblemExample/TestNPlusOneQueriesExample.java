package nPlusOneProblemExample;

import app.entities.Human;
import app.entities.NPlusOneCacheableHuman;
import app.entities.NPlusOneCacheablePet;
import app.entities.Pet;
import global.HibernateTest;
import org.hibernate.Session;
import org.hibernate.jpa.QueryHints;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;


@SuppressWarnings("unused")
public class TestNPlusOneQueriesExample extends HibernateTest {

    @BeforeEach
    @AfterEach
    public void clearAll() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                session.createQuery("from Human", Human.class)
                        .list().forEach(session::remove);
                session.createQuery("from Pet", Pet.class).list().forEach(session::remove);
                session.createQuery("from NPlusOneCacheableHuman", NPlusOneCacheableHuman.class)
                        .list().forEach(session::remove);
            });
        }
    }

    /*
     * 1. Please find '>>>>>>>>>>> start' comment first
     * 2. Below comment and above comment '>>>>>>>>>>> end' above please search for this comment 'org.hibernate.SQL - select'
     * 3. There will be three comment like this because code has n + 1 problem where n = 2 queries for pets and 1 means query for people.
     * */
    @Test
    public void howNPlusOneQueriesLookLike() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var pets1 = List.of(new Pet("cat"), new Pet("caterpillar"), new Pet("fish"));
                var pets2 = List.of(new Pet("bird"), new Pet("mouse"), new Pet("donkey"));
                var human1 = new Human("man1");
                var human2 = new Human("man2");
                human1.setPets(pets1);
                human2.setPets(pets2);
                pets1.forEach(p -> p.setHuman(human1));
                pets2.forEach(p -> p.setHuman(human2));
                pets1.forEach(session::save);
                pets2.forEach(session::save);
                session.save(human1);
                session.save(human2);
            });
        }
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                System.out.println(">>>>>>>>>>> start");
                var numberOfPets = session.createQuery("from Human", Human.class)
                        .list()
                        .stream()
                        .mapToLong(human -> human.getPets().size())//
                        .sum();
                System.out.println(">>>>>>>>>>> end");
            });
        }
    }

    /*
     * 1. Please find '>>>>>>>>>>> start' comment first
     * 2. Below comment and above comment '>>>>>>>>>>> end' above please search for this comment 'org.hibernate.SQL - select'
     * 3. There will be three comment like this. This is because Pet has ManyToOne which is Eager fetch type. This means that
     *    that both Human records are picked up in the process in separate queries.
     * */
    @Test
    public void testNPlusOneWhenManyToOneWithDefaultFetchTypeEager() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var pets1 = List.of(new Pet("cat"), new Pet("caterpillar"));
                var pets2 = List.of(new Pet("bird"), new Pet("mouse"));
                var human1 = new Human("man1");
                var human2 = new Human("man2");
                human1.setPets(pets1);
                human2.setPets(pets2);
                pets1.forEach(p -> p.setHuman(human1));
                pets2.forEach(p -> p.setHuman(human2));
                pets1.forEach(session::save);
                pets2.forEach(session::save);
                session.save(human1);
                session.save(human2);
            });
        }
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                System.out.println(">>>>>>>>>>> start");
                var pets = session.createQuery("from Pet", Pet.class).list();
                System.out.println(">>>>>>>>>>> end");
            });
        }
    }

    @Test
    public void checkHibernateQuerySecondLevelCacheNPluOne() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var human1 = new NPlusOneCacheableHuman("human1");
                var human2 = new NPlusOneCacheableHuman("human2");
                var pets = List.of(new NPlusOneCacheablePet("bird"), new NPlusOneCacheablePet("dog"),
                        new NPlusOneCacheablePet("cat"), new NPlusOneCacheablePet("fish"));
                human1.setPets(pets.subList(0, 1));
                human2.setPets(pets.subList(2, 3));
                pets.subList(0, 1).forEach(p -> p.setHuman(human1));
                pets.subList(2, 3).forEach(p -> p.setHuman(human2));
                session.persist(human1);
                session.persist(human2);
            });
        }
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                System.out.println(">>>>>>>>>>> start1");
                // First query will make three selects on database to gather all needed data
                // 1. select for all pets
                // 2. there are two Human entries so two selects for Human entities
                session.getEntityManagerFactory()
                        .createEntityManager()
                        .createQuery("from NPlusOneCacheablePet p order by p.human.id desc")
                        .setHint(QueryHints.HINT_CACHEABLE, true).getResultList();
                //All data are obtained from second level cache
                session.getEntityManagerFactory()
                        .createEntityManager()
                        .createQuery("from NPlusOneCacheablePet p order by p.human.id desc")
                        .setHint(QueryHints.HINT_CACHEABLE, true).getResultList();
                System.out.println(">>>>>>>>>>> end1");
            });
        }
        //Evict all Human entities from cache
        sessionFactory.getCache().evict(NPlusOneCacheableHuman.class);
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                System.out.println(">>>>>>>>>>> start2");
                //Two selects are made for Human objects
                session.getEntityManagerFactory()
                        .createEntityManager()
                        .createQuery("from NPlusOneCacheablePet p order by p.human.id desc")
                        .setHint(QueryHints.HINT_CACHEABLE, true).getResultList();
                System.out.println(">>>>>>>>>>> end2");
            });
        }
    }
}
