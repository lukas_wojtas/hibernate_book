package nPlusOneProblemExample;

import app.entities.NPlusOneHumanBatchSolution;
import app.entities.NPlusOneHumanEntityGraph;
import app.entities.NPlusOneHumanJPQLJoin;
import app.entities.NPlusOnePetBatchSolution;
import app.entities.NPlusOnePetEntityGraph;
import app.entities.NPlusOnePetJPQLJoin;
import global.HibernateTest;
import org.hibernate.Session;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TestNPlusOneSolutions extends HibernateTest {

    @BeforeEach
    @AfterEach
    public void cleanUp() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> session.createQuery("from NPlusOneHumanBatchSolution", NPlusOneHumanBatchSolution.class)
                    .list().forEach(session::remove));
        }
    }

    /*
     * 1. Please find '>>>>>>>> start' comment first
     * 2. Below comment and above comment '>>>>>>>>>>> end' please search for this comment 'org.hibernate.SQL - select'
     * 3. There will be three comments like this instead of seven because NPlusOneHumanBatchSolution has batch size set up to 3.
     *    Therefore number of queries work up lke this: 1 query for NPlusOneHumanBatchSolution + 2 * 3 queries for 6 pets
     *    because of batch
     * */
    @Test
    public void testBatchApproachToNPlusOneProblem() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var pets = IntStream.range(0, 12)
                        .boxed()
                        .map(i -> new NPlusOnePetBatchSolution("Pet" + i))
                        .collect(Collectors.toList());
                var people = IntStream.range(0, 6)
                        .boxed()
                        .map(i -> new NPlusOneHumanBatchSolution("man" + i))
                        .collect(Collectors.toList());
                people.get(0).setPets(pets.subList(0, 2));
                people.get(1).setPets(pets.subList(2, 4));
                people.get(2).setPets(pets.subList(4, 6));
                people.get(3).setPets(pets.subList(6, 8));
                people.get(4).setPets(pets.subList(8, 10));
                people.get(5).setPets(pets.subList(10, 12));
                pets.subList(0, 2).forEach(p -> p.setHuman(people.get(0)));
                pets.subList(2, 4).forEach(p -> p.setHuman(people.get(1)));
                pets.subList(4, 6).forEach(p -> p.setHuman(people.get(2)));
                pets.subList(6, 8).forEach(p -> p.setHuman(people.get(3)));
                pets.subList(8, 10).forEach(p -> p.setHuman(people.get(4)));
                pets.subList(10, 12).forEach(p -> p.setHuman(people.get(5)));
                people.forEach(session::persist);
            });
        }
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                System.out.println(">>>>>>>> start");
                var people = session.createQuery("from NPlusOneHumanBatchSolution", NPlusOneHumanBatchSolution.class)
                        .list();
                people.forEach(h -> System.out.println("number of pets: " + h.getPets().size()));
                System.out.println(">>>>>>>> end");
            });
        }
    }

    /*
     * 1. Please find '>>>>>>>> start' comment first
     * 2. Below comment and above comment '>>>>>>>>>>> end' please search for this comment 'org.hibernate.SQL - select'
     * 3. There will be one comment because all data are fetched in one go because of join
     * */
    @Test
    public void testNPlusOneSolutionWIthUsingJPQLJoin() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var pets = IntStream.range(0, 12)
                        .boxed()
                        .map(i -> new NPlusOnePetJPQLJoin("Pet " + i))
                        .collect(Collectors.toList());
                var people = IntStream.range(0, 3)
                        .boxed()
                        .map(i -> new NPlusOneHumanJPQLJoin("man " + i))
                        .collect(Collectors.toList());
                people.get(0).setPets(pets.subList(0, 4));
                people.get(1).setPets(pets.subList(4, 8));
                people.get(2).setPets(pets.subList(8, 12));
                pets.subList(0, 4).forEach(p -> p.setHuman(people.get(0)));
                pets.subList(4, 8).forEach(p -> p.setHuman(people.get(1)));
                pets.subList(8, 12).forEach(p -> p.setHuman(people.get(2)));
                people.forEach(session::persist);
            });
        }
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                System.out.println(">>>>>>>> start");
                var people = session.createNamedQuery("findWithPets",
                        NPlusOneHumanJPQLJoin.class).list();
                people.forEach(h -> System.out.println("number of pets: " + h.getPets().size()));
                System.out.println(">>>>>>>> end");
            });
        }
    }

    /*
     * 1. Please find '>>>>>>>> start' comment first
     * 2. Below comment and above comment '>>>>>>>>>>> end' please search for this comment 'org.hibernate.SQL - select'
     * 3. There will be one comment because all data are fetched in one go because of Graph
     * */
    @Test
    public void testNPlusOneFixWithEntityGraph() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var pets = IntStream.range(0, 12)
                        .boxed()
                        .map(i -> new NPlusOnePetEntityGraph("pet" + i))
                        .collect(Collectors.toList());
                var people = IntStream.range(0, 3)
                        .boxed()
                        .map(i -> new NPlusOneHumanEntityGraph("man " + i))
                        .collect(Collectors.toList());
                people.get(0).setPets(pets.subList(0, 4));
                people.get(1).setPets(pets.subList(4, 8));
                people.get(2).setPets(pets.subList(8, 12));
                pets.subList(0, 4).forEach(p -> p.setHuman(people.get(0)));
                pets.subList(4, 8).forEach(p -> p.setHuman(people.get(1)));
                pets.subList(8, 12).forEach(p -> p.setHuman(people.get(2)));
                people.forEach(session::persist);
            });
        }
        try (final Session session = sessionFactory.openSession()) {
            System.out.println(">>>>>>>> start");
            var people = session.getEntityManagerFactory()
                    .createEntityManager()
                    .createQuery("from NPlusOneHumanEntityGraph", NPlusOneHumanEntityGraph.class)
                    .setHint("javax.persistence.fetchgraph", session.getEntityGraph("human-with-pets-graph"))
                    .getResultList();
            people.forEach(h -> System.out.println("number of pets: " + h.getPets().size()));
            System.out.println(">>>>>>>> end");
        }
    }
}
