package isolationLevel;

import app.entities.Person;
import global.HibernateTest;
import isolationLevel.transactions.ReadCommittedTransaction1;
import isolationLevel.transactions.ReadCommittedTransaction2;
import isolationLevel.transactions.ReadCommittedTransaction3;
import isolationLevel.transactions.ReadCommittedTransaction4;
import org.hibernate.Session;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

public class TestIsolationLevelReadCommitted extends HibernateTest {

    private Long id;

    @BeforeEach
    @AfterEach
    public void cleanUp() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> session.createQuery("from Person", Person.class)
                    .list().forEach(session::remove));
        }
    }
    /*
    * Events:
    * =======
    * 1. Starting thread Transaction 2 with session SessionImpl(1274691302<open>)
    * 2. personBefore newName in thread Transaction 2
    * 3. Starting thread Transaction 1 with session SessionImpl(703086468<open>)
    * 4. committing in thread Transaction 1
    * 5. committed in thread Transaction 1
    * 6. personAfter updated in thread Transaction 2
    * 7. committing in thread Transaction 2
    * 8. committed in thread Transaction 2
    *
    * Description:
    * ============
    * In Transaction 2 there are two reads. First read is done with initial 'newValue'.
    * Then in Transaction 2 record is being updated. Afterwards Transaction 1 performs another read (refresh)
    * New value is visible. This is example of committed read isolation level.
    * */
    @Test
    public void checkReadCommittedIsolationLevel() {
        var lock = new Object();
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var person = new Person("newName");
                session.save(person);
                this.id = person.getId();
            });
        }
        var threads = List.of(new Thread(new ReadCommittedTransaction1(lock, this.id), "Transaction 1"),
                new Thread(new ReadCommittedTransaction2(lock, this.id), "Transaction 2"));
        threads.forEach(t -> {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        threads.forEach(Thread::start);
    }

    /*
     * Nature of events
     * ================
     * 1. session in thread Transaction 1 is SessionImpl(475584018<open>)
     * 2. session in thread Transaction 2 is SessionImpl(1217655981<open>)
     * 3. update person in thread Transaction 2 is updated
     * 4. waiting for second thread in thread Transaction 2
     * 5. processing thread Transaction 1
     * 6. update person in thread Transaction 1 is updated
     * 7. committing transaction from thread Transaction 1
     * 8. committed in thread Transaction 1
     * 9. committing transaction from thread Transaction 2
     * 10. committed in thread Transaction 2
     *
     * Description
     * ===========
     * Transaction 1 is updating already predefined record.
     * Transaction 2 can see this record although Transaction 1 is not committed (only flushed).
     * This is because Dirty read isolation level is enabled. Therefore in Transaction 2 second read
     * (refresh) is NOT visible. It is still newValue
     * */
    @Test
    public void checkCommittedWithUncommittedScenario() {
        var lock = new Object();
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var person = new Person("newName");
                session.save(person);
                this.id = person.getId();
            });
        }
        var threads = List.of(new Thread(new ReadCommittedTransaction3(lock, this.id), "Transaction 1"),
                new Thread(new ReadCommittedTransaction4(lock, this.id), "Transaction 2"));
        threads.forEach(Thread::start);
        threads.forEach(t -> {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }
}
