package isolationLevel.transactions;

import app.entities.Person;
import global.HibernateTestTransactionLevel;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;

import java.net.URISyntaxException;

@Slf4j
@AllArgsConstructor
public class RepeatableReadTransaction1 implements Runnable {

    private final Object lock;

    @Override
    public void run() {
        synchronized (lock) {
            try (final var session = HibernateTestTransactionLevel
                    .configureCustomSessionFactoryWithIsolationLevelReadCommitted().openSession()) {
                var tx = session.beginTransaction();
                log.info(">>>>>> Getting list for the first time in thread : {}", Thread.currentThread().getName());
                var people = session.createNativeQuery("select * from people where people.name like '%new%'",
                        Person.class).list();
                log.info(">>>>>> list size is : {}", people.size());
                Assertions.assertEquals(1, people.size());
                lock.notify();
                lock.wait();
                log.info(">>>>>> Getting list for the second time in thread {}", Thread.currentThread().getName());
                session.clear();
                var peopleAfter = session.createNativeQuery("select * from people where people.name like '%new%'",
                        Person.class).list();
                Assertions.assertEquals(3, peopleAfter.size());
                log.info(">>>>>> before commit in thread {}", Thread.currentThread().getName());
                tx.commit();
                log.info(">>>>>> after commit in thread {}", Thread.currentThread().getName());
            } catch (URISyntaxException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
