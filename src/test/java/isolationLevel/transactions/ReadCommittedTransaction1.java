package isolationLevel.transactions;

import app.entities.Person;
import global.HibernateTestTransactionLevel;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.Transaction;

@AllArgsConstructor
@Slf4j
public class ReadCommittedTransaction1 implements Runnable {

    private final Object lock;
    private final Long id;

    @SneakyThrows
    @Override
    public void run() {
        synchronized (lock) {
            try (final Session session = HibernateTestTransactionLevel
                    .configureCustomSessionFactoryWithIsolationLevelReadCommitted().openSession()) {
                lock.wait();
                Transaction tx = session.beginTransaction();
                log.info(">>>>>> Starting thread {} with session {}", Thread.currentThread().getName(), session);
                var person = session.get(Person.class, this.id);
                person.setName("updated");
                session.save(person);
                log.info(">>>>>> committing in thread {}", Thread.currentThread().getName());
                tx.commit();
                log.info(">>>>>> committed in thread {}", Thread.currentThread().getName());
                lock.notify();
            }
        }
    }
}
