package isolationLevel.transactions;

import app.entities.Person;
import global.HibernateTestTransactionLevel;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.jupiter.api.Assertions;

@Slf4j
@AllArgsConstructor
public class ReadCommittedTransaction3 implements Runnable {

    private final Object lock;
    private final Long id;

    @SneakyThrows
    @Override
    public void run() {
        synchronized (lock) {
            try (final Session session = HibernateTestTransactionLevel
                    .configureCustomSessionFactoryWithIsolationLevelReadUncommitted().openSession()) {
                lock.wait();
                log.info(">>>>>>>>>>>>>>>>>>>> session in thread {} is {}", Thread.currentThread().getName(), session);
                Transaction tx = session.beginTransaction();
                var person = session.get(Person.class, id);
                Assertions.assertEquals("newName", person.getName());
                person.setName("updated");
                session.save(person);
                session.flush();
                log.info(">>>>>>>>>>>>>>>>>>>> update person in thread {} is {}",
                        Thread.currentThread().getName(), person.getName());
                lock.notify();
                log.info(">>>>>>>>>>>>>>>>>>>> waiting for second thread in thread {}", Thread.currentThread().getName());
                lock.wait();
                log.info(">>>>>>>>>>>>>>>>>>>> committing transaction from thread {}", Thread.currentThread().getName());
                tx.commit();
                log.info(">>>>>>>>>>>>>>>>>>>> committed in thread {}", Thread.currentThread().getName());
            }
        }
    }
}
