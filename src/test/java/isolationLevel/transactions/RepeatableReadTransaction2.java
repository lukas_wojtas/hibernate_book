package isolationLevel.transactions;

import app.entities.Person;
import global.HibernateTestTransactionLevel;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
@AllArgsConstructor
public class RepeatableReadTransaction2 implements Runnable {

    private final Object lock;

    @SneakyThrows
    @Override
    public void run() {
        synchronized (lock) {
            try (final var session = HibernateTestTransactionLevel
                    .configureCustomSessionFactoryWithIsolationLevelReadCommitted().openSession()) {
                log.info(">>>>>> before wait {}", Thread.currentThread().getName());
                lock.wait();
                var transaction = session.beginTransaction();
                log.info(">>>>>> starting to execute in {}", Thread.currentThread().getName());
                List.of(new Person("newTwo"), new Person("newThree")).forEach(session::save);
                log.info(">>>>>> before commit in {}", Thread.currentThread());
                transaction.commit();
                log.info(">>>>>> after commit in {}", Thread.currentThread().getName());
                lock.notify();
            }
        }
    }
}
