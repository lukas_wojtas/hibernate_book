package isolationLevel.transactions;

import app.entities.Person;
import global.HibernateTestTransactionLevel;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.jupiter.api.Assertions;

@Slf4j
@AllArgsConstructor
public class ReadCommittedTransaction2 implements Runnable {

    private final Object lock;
    private final Long id;

    @SneakyThrows
    @Override
    public void run() {
        synchronized (lock) {
            try (final Session session = HibernateTestTransactionLevel
                    .configureCustomSessionFactoryWithIsolationLevelReadCommitted().openSession()) {
                log.info(">>>>>> Starting thread {} with session {}", Thread.currentThread().getName(), session);
                Transaction tx = session.beginTransaction();
                var personBefore = session.get(Person.class, this.id);
                session.flush();
                log.info(">>>>>> personBefore {} in thread {}", personBefore.getName(), Thread.currentThread().getName());
                Assertions.assertEquals("newName", personBefore.getName());
                lock.notify();
                lock.wait();
                session.refresh(personBefore);
                log.info(">>>>>> personAfter {} in thread {}", personBefore.getName(), Thread.currentThread().getName());
                Assertions.assertEquals("updated", personBefore.getName());
                lock.notify();
                log.info(">>>>>> committing in thread {}", Thread.currentThread().getName());
                tx.commit();
                log.info(">>>>>> committed in thread {}", Thread.currentThread().getName());
            }
        }
    }
}
