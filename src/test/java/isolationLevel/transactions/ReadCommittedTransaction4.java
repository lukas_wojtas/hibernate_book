package isolationLevel.transactions;

import app.entities.Person;
import global.HibernateTestTransactionLevel;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.jupiter.api.Assertions;

@Slf4j
@AllArgsConstructor
public class ReadCommittedTransaction4 implements Runnable {

    private final Object lock;
    private final Long id;

    @SneakyThrows
    @Override
    public void run() {
        synchronized (lock) {
            try (final Session session = HibernateTestTransactionLevel
                    .configureCustomSessionFactoryWithIsolationLevelReadUncommitted().openSession()) {
                log.info(">>>>>>>>>>>>>>>>>>>> session in thread {} is {}", Thread.currentThread().getName(), session);
                Transaction tx = session.beginTransaction();
                var person = session.get(Person.class, id);
                Assertions.assertEquals("newName", person.getName());
                lock.notify();
                lock.wait();
                log.info(">>>>>>>>>>>>>>>>>>>> processing thread {}", Thread.currentThread().getName());
                session.refresh(person);
                log.info(">>>>>>>>>>>>>>>>>>>> update person in thread {} is {}",
                        Thread.currentThread().getName(), person.getName());
                Assertions.assertEquals("updated", person.getName());
                lock.notify();
                log.info(">>>>>>>>>>>>>>>>>>>> committing transaction from thread {}", Thread.currentThread().getName());
                tx.commit();
                log.info(">>>>>>>>>>>>>>>>>>>> committed in thread {}", Thread.currentThread().getName());
            }
        }
    }
}
