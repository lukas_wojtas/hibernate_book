package isolationLevel;

import app.entities.Person;
import global.HibernateTest;
import isolationLevel.transactions.RepeatableReadTransaction1;
import isolationLevel.transactions.RepeatableReadTransaction2;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

public class TestIsolationLevelRepeatableRead extends HibernateTest {

    @BeforeEach
    @AfterEach
    public void cleanUp() {
        try (final var session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                session.createQuery("from Person", Person.class).list().forEach(session::remove);
            });
        }
    }

    @Test
    public void shouldShowPhantomReadInAction() {
        var lock = new Object();
        try (final var session = sessionFactory.openSession()) {
            this.execute(session, () -> session.save(new Person("new Basic")));
        }
        var threads = List.of(new Thread(new RepeatableReadTransaction2(lock), "Transaction 1"),
                new Thread(new RepeatableReadTransaction1(lock), "Transaction 2"));
        threads.forEach(Thread::start);
        threads.forEach(t -> {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }
}
