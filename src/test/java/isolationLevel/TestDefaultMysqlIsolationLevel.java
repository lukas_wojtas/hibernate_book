package isolationLevel;

import global.HibernateTest;
import org.hibernate.Session;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.sql.Connection;

public class TestDefaultMysqlIsolationLevel extends HibernateTest {

    @Test
    public void getMySqlDefaultIsolationLevel() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> session.doWork(
                    connection -> Assertions.assertEquals(Connection.TRANSACTION_REPEATABLE_READ,
                            connection.getTransactionIsolation())));
        }
    }
}
