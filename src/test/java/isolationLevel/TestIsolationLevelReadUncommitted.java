package isolationLevel;

import app.entities.Person;
import global.HibernateTest;
import isolationLevel.transactions.ReadUncommittedTransaction1;
import isolationLevel.transactions.ReadUncommittedTransaction2;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

@Slf4j
public class TestIsolationLevelReadUncommitted extends HibernateTest {

    private Long id;

    @BeforeEach
    @AfterEach
    public void cleanUp() {
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> session.createQuery("from Person", Person.class).list().forEach(session::remove));
        }
    }

    /*
    * Nature of events
    * ================
    * 1. session in thread Transaction 1 is SessionImpl(475584018<open>)
    * 2. session in thread Transaction 2 is SessionImpl(1217655981<open>)
    * 3. update person in thread Transaction 2 is updated
    * 4. waiting for second thread in thread Transaction 2
    * 5. processing thread Transaction 1
    * 6. update person in thread Transaction 1 is updated
    * 7. committing transaction from thread Transaction 1
    * 8. committed in thread Transaction 1
    * 9. committing transaction from thread Transaction 2
    * 10. committed in thread Transaction 2
    *
    * Description
    * ===========
    * Transaction 1 is updating already predefined record.
    * Transaction 2 can see this record although Transaction 1 is not committed (only flushed).
    * This is because Dirty read isolation level is enabled. Therefore in Transaction 2 second read
    * (refresh) is visible.
    * */
    @SneakyThrows
    @Test
    public void checkReadUncommittedIsolationLevel() {
        var lock = new Object();
        try (final Session session = sessionFactory.openSession()) {
            this.execute(session, () -> {
                var person = new Person("newName");
                session.save(person);
                this.id = person.getId();
            });
        }
        var threads = List.of(new Thread(new ReadUncommittedTransaction1(lock, this.id), "Transaction 1"),
                new Thread(new ReadUncommittedTransaction2(lock, this.id), "Transaction 2"));
        threads.forEach(Thread::start);
        threads.forEach(t -> {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }
}
