package chapter8;

import app.entities.Person;
import global.HibernateTestTransactionLevel;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Slf4j
public class TestDeadlockOnTransactions {

    private List<Long> people;

    private SessionFactory sessionFactory;

    @BeforeEach
    @AfterEach
    public void cleanUp() throws URISyntaxException {
        this.sessionFactory = HibernateTestTransactionLevel.configureCustomSessionFactoryWithIsolationLevelSerializable();
        final var p1 = new Person("A");
        final var p2 = new Person("B");
        try (var session = sessionFactory.openSession()) {
            var tx = session.beginTransaction();
            session.createQuery("from Person", Person.class).getResultList().forEach(session::remove);
            session.persist(p1);
            session.persist(p2);
            tx.commit();
        }
        Assertions.assertNotNull(p1.getId());
        Assertions.assertNotNull(p2.getId());
        this.people = Arrays.asList(p1.getId(), p2.getId());
    }

    /*
     * Exception is visible in console log
     * */
    @SuppressWarnings("unused")
    @Test
    public void shouldTriggerDeadlock() throws InterruptedException {
        var executor = Executors.newFixedThreadPool(2);
        executor.submit(() -> this.updatePublishers("Session 1", this.people.get(0), this.people.get(1)));
        executor.submit(() -> this.updatePublishers("Session 2", this.people.get(1), this.people.get(0)));
        executor.shutdown();
        if (!executor.awaitTermination(60, TimeUnit.SECONDS)) {
            executor.shutdownNow();
            if (!executor.awaitTermination(60, TimeUnit.SECONDS)) {
                System.out.println("Executor did not terminate");
            }
        }
        try (var session = sessionFactory.openSession()) {
            var query = session.createQuery("from Person p order by p.name",
                    Person.class);
            String result = query
                    .list()
                    .stream()
                    .map(Person::getName)
                    .collect(Collectors.joining(","));
        }
    }

    private void updatePublishers(final String prefix, Long... ids) {
        try (var session = sessionFactory.openSession()) {
            var tx = session.beginTransaction();
            for (Long id : ids) {
                TimeUnit.MILLISECONDS.sleep(100);
                var person = session
                        .byId(Person.class)
                        .load(id);
                person.setName(prefix + " " + person.getName());
            }
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }
}
