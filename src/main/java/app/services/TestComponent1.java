
package app.services;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Service
@Slf4j
public class TestComponent1 {

    private final Session session;

    @Autowired
    public TestComponent1(Session session) {
        this.session = session;
    }

    @PostConstruct
    void postConstruct() {
        log.info("Session ref in " + TestComponent1.class.getName() + " is " + this.session);
    }

    @PreDestroy
    void preDestroy() {
        log.info("pre destroy called for class " + TestComponent1.class.getName());
        if (this.session.isOpen()) {
            this.session.close();
        }
    }
}
