package app.services;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;

@Service
@Slf4j
public class TestComponent2 {

    private final Session session;

    @Autowired
    public TestComponent2(Session session) {
        this.session = session;
    }

    @PreDestroy
    public void preDestroy() {
        log.info("pre destroy called for class " + TestComponent2.class.getName());
        if (this.session.isOpen()) {
            session.close();
        }
    }
}
