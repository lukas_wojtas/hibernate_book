package app.config;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import javax.sql.DataSource;

@Configuration
public class HibernateConfig {
    /*
    * Pure hibernate configuration from xml
    * */
    @Bean
    public SessionFactory sessionFactory() {
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure()
                .build();
        return new MetadataSources(registry).buildMetadata().buildSessionFactory();
    }
    /*
    * Every call for bean creates new session.
    * */
    @Bean
    @Scope(scopeName = "prototype")
    public Session getSession() {
        return sessionFactory().openSession();
    }
    /*
     * This is needed for spring to run
     * */
    @Bean
    public DataSource datasource() {
        return DataSourceBuilder.create().build();
    }
}
