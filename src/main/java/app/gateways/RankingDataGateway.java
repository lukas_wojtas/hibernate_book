package app.gateways;

import app.entities.Ranking;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class RankingDataGateway {

    public Long saveRanking(Session session, Ranking ranking) {
        return (Long) session.save(ranking);
    }

    public void persistRanking(Session session, Ranking ranking) {
        session.persist(ranking);
    }

    public List<Ranking> getRankingBySubjectAndSkill(Session session, String subjectName, String skillName) {
        Query<Ranking> query = session
                .createQuery("from Ranking r where r.subject.name=:subjectName and r.skill.name=:skillName", Ranking.class);
        query.setParameter("subjectName", subjectName);
        query.setParameter("skillName", skillName);
        return query.list();
    }

    public Optional<Ranking> getRankingBySubjectAndObserverAndSkill(Session session, String subjectName,
                                                                    String observerName, String skillName) {
        final Query<Ranking> query = session.createQuery("from Ranking r where r.subject.name=:subjectName and " +
                "r.observer.name=:observerName and r.skill.name=:skillName", Ranking.class);
        query.setParameter("subjectName", subjectName);
        query.setParameter("observerName", observerName);
        query.setParameter("skillName", skillName);
        return Optional.ofNullable(query.uniqueResult());
    }

    public double getAverageRankingForSubject(Session session, String subjectName, String skillName) {
        final Query<Ranking> query = session.createQuery("from Ranking r where r.subject.name=:subjectName and " +
                "r.skill.name=:skillName", Ranking.class);
        query.setParameter("subjectName", subjectName);
        query.setParameter("skillName", skillName);
        IntSummaryStatistics collect = query
                .list()
                .stream()
                .collect(Collectors.summarizingInt(Ranking::getRanking));
        return collect.getAverage();
    }

    public void deleteRanking(Session session, List<Ranking> rankings) {
        rankings.forEach(r -> session.delete(Ranking.class.getName(), r));
    }

    public List<Ranking> getAllRankingsForSubject(Session session, String subject) {
        Query<Ranking> query = session.createQuery("from Ranking r where r.subject.name=:subject", Ranking.class);
        query.setParameter("subject", subject);
        return query.list();
    }

    public int findBestForGivenSkill(Session session, String skillName) {
        Query<Ranking> query = session.createQuery("from Ranking r where r.skill.name=:skillName", Ranking.class);
        query.setParameter("skillName", skillName);
        return query.list().stream().map(Ranking::getRanking).max((oldValue, newValue) -> {
            if (oldValue > newValue) {
                return 1;
            } else if (oldValue.equals(newValue)) {
                return 0;
            }
            return -1;
        }).orElse(-1);
    }
}
