package app.gateways;

import app.entities.Person;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;
import java.util.Optional;

/*
 * Table Data gateway pattern
 * */
public class PersonDataGateway {

    public List<Person> getPeopleByName(Session session, String name) {
        Query<Person> query = session
                .createQuery("from Person p where p.name=:name", Person.class);
        query.setParameter("name", name);
        return query.list();
    }

    public Long savePersonWithUniqueName(Session session, Person person) {
        Optional<Person> personOptional = this.getPeopleByName(session, person.getName()).stream().findFirst();
        if (personOptional.isEmpty()) {
            System.out.println(">>>>>>>>>>>>>");
            return (Long) session.save(person);
        }
        return personOptional.get().getId();
    }

    /**
     * Persist should be done in transaction scope!!
     */
    public void persistPersonWithUniqueName(Session session, Person person) {
        if (this.getPeopleByName(session, person.getName()).stream().findFirst().isEmpty()) {
            session.persist(person);
        }
    }
}
