package app.gateways;

import app.entities.Skill;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.Optional;

/*
 * Table gateway pattern
 * */
public class SkillDataGateway {

    public Optional<Skill> getSkillByName(Session session, String name) {
        Query<Skill> query = session.createQuery("from Skill s where s.name=:name", Skill.class);
        query.setParameter("name", name);
        return Optional.ofNullable(query.uniqueResult());
    }

    public Long saveSkill(Session session, Skill skill) {
        Optional<Skill> optionalSkill = getSkillByName(session, skill.getName());
        if (optionalSkill.isEmpty()) {
            return (Long) session.save(skill);
        }
        return optionalSkill.get().getId();
    }
    /*
    * Persist need to be called in transaction scope
    * */
    public void persistSkill(Session session, Skill skill) {
        if (this.getSkillByName(session, skill.getName()).isEmpty()) {
            session.persist(skill);
        }
    }
}
