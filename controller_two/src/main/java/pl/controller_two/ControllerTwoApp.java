package pl.controller_two;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Collections;

@SpringBootApplication
public class ControllerTwoApp {

    private static final String PROP_NAME = "server.port";
    private static final String PORT = "8081";

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(ControllerTwoApp.class);
        app.setDefaultProperties(Collections
                .singletonMap(PROP_NAME, PORT));
        app.run(args);
    }
}
