package pl.controller_two.controller;

import app.dto.HumanDto;
import app.dto.PetDto;
import app.entities.NPlusOneCacheableHuman;
import app.entities.NPlusOneCacheablePet;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController(value = "two")
public class RestControllerTwo {

    private final List<Long> petIdList = new ArrayList<>();
    private final List<Long> humanIdList = new ArrayList<>();

    @Autowired
    private Session session;

    @GetMapping(path = "two_l2_list_pets")
    public List<PetDto> getAllPetsL2Cache() {
        if (this.petIdList.isEmpty()) {
            this.setUpData();
        }
        var tx = session.beginTransaction();
        var pets = this.petIdList
                .stream()
                .map(id -> session.find(NPlusOneCacheablePet.class, id))
                .map(p -> new PetDto(p.getName(), p.getHuman().getName()))
                .collect(Collectors.toList());
        tx.commit();
        session.close();
        return pets;
    }

    @GetMapping(path = "two_l2_get_people")
    public List<HumanDto> getHumansL2Cache() {
        if (this.humanIdList.isEmpty()) {
            this.setUpData();
        }
        var tx = session.beginTransaction();
        var people = this.humanIdList
                .stream()
                .map(id -> session.find(NPlusOneCacheableHuman.class, id))
                .map(h -> new HumanDto(h.getName(), h.getPets()
                        .stream()
                        .map(NPlusOneCacheablePet::getName)
                        .collect(Collectors.toList())))
                .collect(Collectors.toList());
        tx.commit();
        session.close();
        return people;
    }

    @GetMapping(path = "two_query_list_pets")
    public List<PetDto> getAllPets() {
        var tx = session.beginTransaction();
        var pets = this.session.createQuery("from NPlusOneCacheablePet", NPlusOneCacheablePet.class)
                .setCacheable(true).setCacheRegion("query.allPets")
                .list()
                .stream()
                .map(p -> new PetDto(p.getName(), p.getHuman().getName()))
                .collect(Collectors.toList());
        tx.commit();
        session.close();
        return pets;
    }

    @GetMapping(path = "two_query_get_people")
    public List<HumanDto> getHumansQuery() {
        var tx = session.beginTransaction();
        var people = this.session.createQuery("from NPlusOneCacheableHuman",
                NPlusOneCacheableHuman.class)
                .setCacheable(true).setCacheRegion("query.allPeople")
                .list()
                .stream()
                .map(h -> new HumanDto(h.getName(), h.getPets()
                        .stream()
                        .map(NPlusOneCacheablePet::getName)
                        .collect(Collectors.toList())))
                .collect(Collectors.toList());
        tx.commit();
        session.close();
        return people;
    }

    private void setUpData() {
        initializeIdTables();
        var tx = session.beginTransaction();
        if (this.petIdList.isEmpty() && this.humanIdList.isEmpty()) {
            var human = new NPlusOneCacheableHuman("name");
            var pets = Arrays.asList(new NPlusOneCacheablePet("bird"),
                    new NPlusOneCacheablePet("cat"), new NPlusOneCacheablePet("dog"));
            human.setPets(pets);
            pets.forEach(p -> p.setHuman(human));
            session.persist(human);
            pets.forEach(session::persist);
            initializeIdTables();
        }
        tx.commit();
    }

    private void initializeIdTables() {
        session.createQuery("from NPlusOneCacheablePet", NPlusOneCacheablePet.class)
                .list()
                .forEach(p -> this.petIdList.add(p.getId()));
        session.createQuery("from NPlusOneCacheableHuman", NPlusOneCacheableHuman.class)
                .list()
                .forEach(h -> this.humanIdList.add(h.getId()));
    }
}
