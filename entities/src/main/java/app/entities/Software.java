package app.entities;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "products_software")
@Data
@NoArgsConstructor
@SuppressWarnings("unused")
public class Software extends Product {

    @Builder
    public Software(Long id, String name, String description, Double price, Supplier supplier, String version) {
        super(id, name, description, price, supplier);
        this.version = version;
    }

    public static class SoftwareBuilder extends ProductBuilder {
        SoftwareBuilder() {
            super();
        }
    }

    @Column
    private String version;
}
