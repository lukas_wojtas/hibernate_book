package app.entities;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.Column;

@javax.persistence.MappedSuperclass
@NoArgsConstructor
@AllArgsConstructor
public abstract class MappedSuperclass {

    @Column
    @NonNull
    private String parent;
}
