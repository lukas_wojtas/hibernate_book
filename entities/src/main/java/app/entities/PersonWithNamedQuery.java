package app.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.hibernate.jpa.QueryHints;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;

@Entity
@Table(name = "people")
@Data
@RequiredArgsConstructor
@NoArgsConstructor
@NamedQueries({
        @NamedQuery(
                name = "findByName",
                query = "from PersonWithNamedQuery where name=:name",
                hints = {
                        @QueryHint(
                                name = QueryHints.HINT_READONLY,
                                value = "true")
                }),
        @NamedQuery(name = "findAll", query = "from PersonWithNamedQuery"),
        @NamedQuery(name = "findById", query = "from PersonWithNamedQuery where id=:id")
})
public class PersonWithNamedQuery {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NonNull
    @Column
    private String name;
}
