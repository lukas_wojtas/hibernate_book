package app.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "join_table_inheritance_strategy_secondary")
@NoArgsConstructor
@Data
public class JoinTableInheritanceStrategyChild extends JoinTableInheritanceStrategyParent{

    public JoinTableInheritanceStrategyChild(String title, String language) {
        super(title);
        this.language = language;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    @NonNull
    private String language;
}
