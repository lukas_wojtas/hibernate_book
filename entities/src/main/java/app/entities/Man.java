package app.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "man")
@NoArgsConstructor
@RequiredArgsConstructor
@Data
public class Man {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @Column
    private String name;

    @NonNull
    @Column
    private String surname;

    @NonNull
    @ElementCollection
    @CollectionTable(name = "man_addresses", joinColumns = @JoinColumn(name = "id"))
    private List<ManAddress> addresses;

    @NonNull
    @ElementCollection
    @CollectionTable(name = "man_phone_numbers", joinColumns = @JoinColumn(name = "id"))
    @Column(name = "phone_number")
    private Set<String> phones;
}
