package app.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SecondaryTable;
import javax.persistence.Table;

/*
* When using secondary table, in real database values are stored only in secondary table. However When using hibernate, all
* row in both tables have values
* */
@Entity
@Table(name = "users")
@NoArgsConstructor
@RequiredArgsConstructor
@SecondaryTable(name = "user_details")
@Data
public class UserWithSecondaryTable {
    @Id
    @NonNull
    private String name;

    @Column(table = "user_details")
    @NonNull
    private Integer age;
    @NonNull
    @Column(table = "user_details")
    private String surname;
}
