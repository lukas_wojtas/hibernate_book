package app.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.time.LocalDate;

@Entity
@Table(name = "books")
@NoArgsConstructor
@RequiredArgsConstructor
@Getter
public class BookWithPublicationDate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter
    private Long id;

    @NonNull
    @Column
    @Setter
    private String title;

    @ManyToOne
    @JoinColumn(name = "libraryId")
    @Setter
    private Library library;

    @Transient
    @Getter
    @NonNull
    private LocalDate publicationDate;
}
