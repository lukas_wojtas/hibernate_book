package app.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import java.util.Set;

@Entity
@Table(name = "articles")
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class Article {
    @Id
    @TableGenerator(name = "article_sequence",
            table = "custom_sequence_table",
            pkColumnName = "name_of_sequence",
            valueColumnName = "next_primary_key")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "article_sequence")
    @Column(name = "article_id")
    private Long id;

    @NonNull
    @Column
    private String title;

    @ManyToMany(mappedBy = "articles")
    private Set<Newspaper> newspapers;
}
