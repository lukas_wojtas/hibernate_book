package app.entities;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "emails")
@NoArgsConstructor
@RequiredArgsConstructor
@Data
@EqualsAndHashCode
public class EmailWithValidation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    @NonNull
    @Email
    @NotNull
    private String email;

    @OneToOne(targetEntity = MessageWithValidation.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "messageId")
    @EqualsAndHashCode.Exclude
    private MessageWithValidation messageInEmail;
}
