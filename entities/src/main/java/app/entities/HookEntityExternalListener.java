package app.entities;

import javax.persistence.PrePersist;
import java.util.HashSet;
import java.util.Set;

@SuppressWarnings("unused")
public class HookEntityExternalListener {

    private static Set<String> events = new HashSet<>();

    @PrePersist
    public void beforePersist(HookEntityWithExternalHook hook) {
        events.add("beforePersist");
    }

    public static Set<String> getEvents() {
        return events;
    }
}
