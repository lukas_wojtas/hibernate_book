package app.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "hook_checks")
@Data
@NoArgsConstructor
@RequiredArgsConstructor
@SuppressWarnings("unused")
public class HookEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    @NonNull
    private String value;

    @Column(name = "hook_before")
    private String beforeHook;

    @Column(name = "hook_after")
    private String afterHook;

    @Transient
    private Set<String> hooks = new HashSet<>();

    @PrePersist
    public void beforePersist() {

        hooks.add("beforePersist");
    }

    @PostPersist
    public void afterPersist() {
        hooks.add("afterPersist");
    }

    @PreUpdate
    public void beforeUpdate() {
        hooks.add("beforeUpdate");
    }

    @PostUpdate
    public void afterUpdate() {
        hooks.add("afterUpdate");
    }

    @PostLoad
    public void afterLoad() {
        hooks.add("afterLoad");
    }

    @PreRemove
    public void beforeRemove() {
        hooks.add("beforeRemove");
    }

    @PostRemove
    public void afterRemove() {
        hooks.add("afterRemove");
    }
}
