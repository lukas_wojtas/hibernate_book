package app.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
@Table(name = "rankings")
@Entity
@ToString
@Data
public class Ranking {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NonNull
    @OneToOne
    @JoinColumn(name = "subject")
    private Person subject;
    @NonNull
    @OneToOne
    @JoinColumn(name = "observer")
    private Person observer;
    @NonNull
    @OneToOne
    @JoinColumn(name = "skill")
    private Skill skill;
    @Column
    @NonNull
    private Integer ranking;
}
