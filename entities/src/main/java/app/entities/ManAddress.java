package app.entities;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Table;

@Embeddable
@Table(name = "man_addresses")
@NoArgsConstructor
@RequiredArgsConstructor
public class ManAddress {

    @Column
    @NonNull
    private String street;

    @Column
    @NonNull
    private String code;

}
