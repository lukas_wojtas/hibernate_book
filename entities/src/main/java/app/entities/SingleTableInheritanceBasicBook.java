package app.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "single_table_inheritance_book")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@NoArgsConstructor
@RequiredArgsConstructor
@Data
@DiscriminatorColumn(name = "book_type", discriminatorType = DiscriminatorType.INTEGER)
@DiscriminatorValue("1")
public class SingleTableInheritanceBasicBook {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    @NonNull
    private String title;
}
