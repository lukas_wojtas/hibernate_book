package app.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import java.util.Set;

@Entity
@Table(name = "newspapers")
@Data
@NoArgsConstructor
@RequiredArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Newspaper {

    @Id
    @TableGenerator(name = "paper_sequence",
            table = "custom_sequence_table",
            pkColumnName = "name_of_sequence",
            valueColumnName = "next_primary_key")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "paper_sequence")
    @Column(name = "newspaper_id")
    private Long id;

    @NonNull
    @Column
    private String title;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "newspaper_to_article_mapping",
            joinColumns = {@JoinColumn(name = "newspaper_id")},
            inverseJoinColumns = {@JoinColumn(name = "article_id")})
    private Set<Article> articles;
}
