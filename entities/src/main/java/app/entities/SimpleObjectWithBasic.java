package app.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Support for LAZY fetching on Basic is optional in JPA, so some JPA providers may not support it.
 * */
@Entity
@Table(name = "simple_objects")
@Data
@RequiredArgsConstructor
@NoArgsConstructor
public class SimpleObjectWithBasic {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Basic(fetch = FetchType.LAZY)
    @NonNull
    @Column(name = "key_value")
    private String key;

    @Basic(fetch = FetchType.LAZY)
    @NonNull
    private Integer value;
}
