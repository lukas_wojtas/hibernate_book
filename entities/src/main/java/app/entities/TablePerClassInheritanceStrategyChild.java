package app.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "table_per_class_inheritance_child")
@NoArgsConstructor
@Data
public class TablePerClassInheritanceStrategyChild extends TablePerClassInheritanceStrategyParent {

    public TablePerClassInheritanceStrategyChild(String title, String language) {
        super(title);
        this.language = language;
    }

    @Column
    private String language;
}
