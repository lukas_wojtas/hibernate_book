package app.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/*
* In second approach because in entity UserCompositeKey2 key is decorated with
* @EmbeddedId this class do not have to have @Embeddable decorator.
* */

@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode
public class UserCompositeId2 implements Serializable {
    private String name;
    private String surname;
}
