package app.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "user_details", uniqueConstraints = {@UniqueConstraint(columnNames = "surname")})
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDetails {

    @Id
    private String name;

    @Column
    private String surname;

    @Column
    private Integer age;
}
