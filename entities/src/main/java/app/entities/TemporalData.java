package app.entities;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Calendar;
import java.util.Date;

@Entity
@Table(name = "temporal_data")
@NoArgsConstructor
@RequiredArgsConstructor
public class TemporalData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    @NonNull
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar dateTime;

    @Column
    @NonNull
    @Temporal(TemporalType.DATE)
    private Date date;

    @Column
    @NonNull
    @Temporal(TemporalType.TIME)
    private Date time;
}
