package app.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "users")
@NoArgsConstructor
@RequiredArgsConstructor
@IdClass(UserCompositeKey3.UserCompositeId3.class)
@Data
public class UserCompositeKey3 {

    @Id
    @NonNull
    private String name;

    @Id
    @NonNull
    private String surname;

    @Column
    @NonNull
    private Integer age;

    @Embeddable
    static class UserCompositeId3 implements Serializable {
        private String name;
        private String surname;
    }
}
