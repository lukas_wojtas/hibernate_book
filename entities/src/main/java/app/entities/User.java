package app.entities;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Table users do not have primary key, so composite key is used instead
 * */
@Entity
@Table(name = "users")
@NoArgsConstructor
@RequiredArgsConstructor
public class User implements Serializable {

    @Id
    @NonNull
    private String name;
    @Id
    @NonNull
    private Integer age;
    @NonNull
    @Column
    private String surname;
}
