package app.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Entity
@Table(name = "single_table_inheritance_book")
@Data
@DiscriminatorValue("2")
public class SingleTableInheritanceItBook extends SingleTableInheritanceBasicBook {

    public SingleTableInheritanceItBook(String title, String language) {
        super(title);
        this.language = language;
    }

    @Column
    private String language;
}
