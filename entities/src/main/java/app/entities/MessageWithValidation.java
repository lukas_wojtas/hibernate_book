package app.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "messages")
@NoArgsConstructor
@RequiredArgsConstructor
@Data
public class MessageWithValidation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @Column
    @Size(min = 1, max = 10)
    private String text;

    @Column
    @NotNull
    @NotBlank
    private String message;

    @Transient
    @Max(20)
    @Min(10)
    @NonNull
    private Integer testNumber;

    @OneToOne(mappedBy = "messageInEmail")
    private EmailWithValidation email;

    public MessageWithValidation(String text, String msg, Integer testNumber) {
        this.text = text;
        this.message = msg;
        this.testNumber = testNumber;
    }
}
