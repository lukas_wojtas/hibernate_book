package app.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name = "table_per_class_inheritance_parent")
@NoArgsConstructor
@RequiredArgsConstructor
@Data
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class TablePerClassInheritanceStrategyParent {

    @Id
    @TableGenerator(name = "super_duper_table_sequence",
            table = "custom_sequence_table",
            pkColumnName = "name_of_sequence",
            valueColumnName = "next_primary_key")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "super_duper_table_sequence")
    private Long id;

    @Column(name = "title")
    @NonNull
    private String title;
}
