package app.entities;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserCompositeKey {

    @Id
    private UserCompositeId compositeId;

    @Column
    private Integer age;
}
