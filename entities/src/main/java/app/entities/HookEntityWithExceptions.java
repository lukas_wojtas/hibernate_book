package app.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PostPersist;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "hook_checks")
@Data
@NoArgsConstructor
@RequiredArgsConstructor
@SuppressWarnings("unused")
public class HookEntityWithExceptions {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    @NonNull
    private String value;

    @Column(name = "hook_before")
    private String beforeHook;

    @Column(name = "hook_after")
    private String afterHook;

    @NonNull
    @Transient
    private Boolean throwBefore;

    @NonNull
    @Transient
    private Boolean throwAfter;

    @PrePersist
    public void beforePersist() {
        if(throwBefore) {
            throw new RuntimeException("before Hook");
        }
    }

    @PostPersist
    public void postPersist() {
        if(throwAfter) {
            throw new RuntimeException("after Hook");
        }
    }
}
