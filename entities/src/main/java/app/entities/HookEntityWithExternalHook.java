package app.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "hook_checks")
@Data
@NoArgsConstructor
@RequiredArgsConstructor
@EntityListeners({HookEntityExternalListener.class})
public class HookEntityWithExternalHook {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    @NonNull
    private String value;

    @Column(name = "hook_before")
    private String beforeHook;

    @Column(name = "hook_after")
    private String afterHook;
}
