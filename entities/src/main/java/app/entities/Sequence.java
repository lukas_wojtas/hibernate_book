package app.entities;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

/*
 * MySql do not support @SequenceGenerator, therefore only Table sequence example is provided
 */
@Entity
@Table(name = "sequence_record")
@NoArgsConstructor
@RequiredArgsConstructor
public class Sequence {

    /*
    * Auto is default, here only to mark that AUTO is used, it actually can be replaced with GenerationType.TABLE
    * */
    @Id
    @TableGenerator(name = "super_duper_table_sequence",
            table = "custom_sequence_table",
            pkColumnName = "name_of_sequence",
            valueColumnName = "next_primary_key")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "super_duper_table_sequence")
    private Long seqId;

    @Column
    @NonNull
    private String value;
}
