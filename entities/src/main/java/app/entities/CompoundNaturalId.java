package app.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.NaturalId;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "compound_natural_ids")
@RequiredArgsConstructor
@NoArgsConstructor
@Data
public class CompoundNaturalId {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NaturalId
    @Column(name = "comp_id1")
    @NonNull
    private String compoundId1;

    @NaturalId
    @Column(name = "comp_id2")
    @NonNull
    private String compoundId2;

    @Column
    @NonNull
    private String value;
}
