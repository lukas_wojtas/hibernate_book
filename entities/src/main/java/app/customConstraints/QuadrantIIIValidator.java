package app.customConstraints;

import app.entities.CoordinatesWithCustomValidation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class QuadrantIIIValidator implements ConstraintValidator<NoQuadrantIII, CoordinatesWithCustomValidation> {
    @Override
    public boolean isValid(CoordinatesWithCustomValidation value, ConstraintValidatorContext context) {
        return value.getLatitude() > 0 && value.getLongitude() > 0;
    }
}
