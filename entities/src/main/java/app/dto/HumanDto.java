package app.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class HumanDto {
    private final String name;
    private final List<String> pets;
}
