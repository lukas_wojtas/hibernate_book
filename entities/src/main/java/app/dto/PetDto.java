package app.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class PetDto {
    private final String name;
    private final String petOwner;
}
