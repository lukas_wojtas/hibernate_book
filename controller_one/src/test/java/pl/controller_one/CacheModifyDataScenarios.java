package pl.controller_one;

import app.entities.NPlusOneCacheableHuman;
import app.entities.NPlusOneCacheablePet;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.Optional;

@SuppressWarnings("unused")
@Slf4j
@SpringBootTest
public class CacheModifyDataScenarios {

    private static final String WHALE = "whale";
    private static final String BLUE_WHALE = "blueWhale";
    private static final String KILLER_WHALE = "killerWhale";

    @Autowired
    private SessionFactory sessionFactory;

    @BeforeEach
    public void initAndDestroy() {
        try (final var session = sessionFactory.openSession()) {
            var tx = session.beginTransaction();
            var whale = session
                    .createQuery("from NPlusOneCacheablePet p where p.name=:name", NPlusOneCacheablePet.class)
                    .setParameter("name", WHALE)
                    .uniqueResult();
            Optional.ofNullable(whale).ifPresent(session::remove);
            var blueWhale = session
                    .createQuery("from NPlusOneCacheablePet p where p.name=:name", NPlusOneCacheablePet.class)
                    .setParameter("name", BLUE_WHALE)
                    .uniqueResult();
            Optional.ofNullable(blueWhale).ifPresent(session::delete);
            var killerWhale = session
                    .createQuery("from NPlusOneCacheablePet p where p.name=:name", NPlusOneCacheablePet.class)
                    .setParameter("name", KILLER_WHALE)
                    .uniqueResult();
            Optional.ofNullable(killerWhale).ifPresent(session::delete);
            tx.commit();
        }
        this.sessionFactory.getStatistics().clear();
        this.sessionFactory.getCache().evictAll();
    }

    @Test
    public void shouldTestQueryCacheUpdate() {
        /*
         * First query perform actual 2 queries because n + 1 on purpose
         * */
        try (final var session = sessionFactory.openSession()) {
            var tx = session.beginTransaction();
            var pets = session.createQuery("from NPlusOneCacheablePet", NPlusOneCacheablePet.class)
                    .setCacheable(true)
                    .list();
            tx.commit();
            Assertions.assertEquals(1, session.getSessionFactory().getStatistics().getQueryExecutionCount());
        }
        /*
         * Second query is taken from Query cache, all entities are already in L2 cache so no query at all
         * */
        try (final var session = sessionFactory.openSession()) {
            var tx = session.beginTransaction();
            var pets = session.createQuery("from NPlusOneCacheablePet", NPlusOneCacheablePet.class)
                    .setCacheable(true)
                    .list();
            tx.commit();
            Assertions.assertEquals(1, session.getSessionFactory().getStatistics().getQueryExecutionCount());
        }
        /*
         * Insert new record, this evict cache and store whale entity in the L2 cache
         * */
        try (final var session = sessionFactory.openSession()) {
            var tx = session.beginTransaction();
            var whale = new NPlusOneCacheablePet(WHALE);
            session.save(whale);
            tx.commit();
            Assertions.assertEquals(1, session.getSessionFactory().getStatistics().getQueryExecutionCount());
        }
        /*
         * Since query cache is evicted, new query for all pets is performed
         * */
        try (final var session = sessionFactory.openSession()) {
            var tx = session.beginTransaction();
            var pets = session.createQuery("from NPlusOneCacheablePet", NPlusOneCacheablePet.class)
                    .setCacheable(true)
                    .list();
            tx.commit();
            Assertions.assertEquals(2, session.getSessionFactory().getStatistics().getQueryExecutionCount());
        }
    }

    @Test
    public void shouldTestUpdateOfL2Cache() {
        long whaleId;
        /*
         * Insert invalidate data in L2 cache, query is performed, cache is invalidated
         * */
        try (final var session = sessionFactory.openSession()) {
            var tx = session.beginTransaction();
            var pet = new NPlusOneCacheablePet(WHALE);
            session.persist(pet);
            tx.commit();
            whaleId = pet.getId();
        }
        /*
         * Read data, query is performed, entity is stored in cache
         * */
        try (final var session = sessionFactory.openSession()) {
            var tx = session.beginTransaction();
            var whale = session.get(NPlusOneCacheablePet.class, whaleId);
            Assertions.assertNotNull(whale);
            tx.commit();
        }
        /*
         * Read data, entity is retrieved from cache
         * */
        try (final var session = sessionFactory.openSession()) {
            var tx = session.beginTransaction();
            var whale = session.get(NPlusOneCacheablePet.class, whaleId);
            Assertions.assertNotNull(whale);
            tx.commit();
        }
        /*
         * Update data, query is performed, result is stored in cache
         * */
        try (final var session = sessionFactory.openSession()) {
            var tx = session.beginTransaction();
            var whale = session.find(NPlusOneCacheablePet.class, whaleId);
            whale.setName(BLUE_WHALE);
            tx.commit();
        }
        /*
         * Read data, entity is retrieved from cache
         * */
        try (final var session = sessionFactory.openSession()) {
            var tx = session.beginTransaction();
            var whale = session.find(NPlusOneCacheablePet.class, whaleId);
            Assertions.assertEquals(BLUE_WHALE, whale.getName());
            tx.commit();
        }
    }

    @Test
    public void testL2AndQueryCacheInteractingWitchEachOther() {
        long petId;
        /*
         * Insert record, query is performed, cache is invalidated
         * */
        try (final var session = sessionFactory.openSession()) {
            final var tx = session.beginTransaction();
            final var pet = new NPlusOneCacheablePet(WHALE);
            session.save(pet);
            tx.commit();
        }
        /*
         * Select query, query is performed, results are stored in cache
         * */
        try (final var session = sessionFactory.openSession()) {
            final var tx = session.beginTransaction();
            petId = session.createQuery("from NPlusOneCacheablePet p where p.name=:name", NPlusOneCacheablePet.class)
                    .setParameter("name", WHALE)
                    .setCacheable(true)
                    .uniqueResult()
                    .getId();
            tx.commit();
        }
        /*
         * Find by is query, result returned from cache, query not performed
         * */
        try (final var session = sessionFactory.openSession()) {
            final var tx = session.beginTransaction();
            session.find(NPlusOneCacheablePet.class, petId);
            tx.commit();
        }
    }

    @Test
    public void shouldTestHowUpdateOnCollectionIsHandledInCache() {
        long humanId;
        /*
        * Creating human with two pets, as a result three queries are executed, all cache entries are invalidated
        * */
        try (final var session = sessionFactory.openSession()) {
            final var tx = session.beginTransaction();
            final var pets = Arrays.asList(new NPlusOneCacheablePet(WHALE), new NPlusOneCacheablePet(BLUE_WHALE));
            final var human = new NPlusOneCacheableHuman("name");
            pets.forEach(p -> p.setHuman(human));
            human.setPets(pets);
            session.save(human);
            humanId = human.getId();
            tx.commit();
        }
        /*
        * getting human performed, two queries executed as a result
        * */
        try (final var session = sessionFactory.openSession()) {
            final var tx = session.beginTransaction();
            final var human = session.find(NPlusOneCacheableHuman.class, humanId);
            Assertions.assertEquals(2, human.getPets().size());
            tx.commit();
        }
        /*
        * Updating collection cache, one query performed, pet cache is invalidated
        * */
        try (final var session = sessionFactory.openSession()) {
            final var tx = session.beginTransaction();
            final var human = session.find(NPlusOneCacheableHuman.class, humanId);
            final var killerWhale = new NPlusOneCacheablePet(KILLER_WHALE);
            human.getPets().add(killerWhale);
            killerWhale.setHuman(human);
            tx.commit();
        }
        /*
        * One query performed, pet cache is loaded
        * */
        try (final var session = sessionFactory.openSession()) {
            final var tx = session.beginTransaction();
            final var human = session.find(NPlusOneCacheableHuman.class, humanId);
            Assertions.assertEquals(3, human.getPets().size());
            tx.commit();
        }
        /*
        *  No db query, all entities loaded from cache
        * */
        try (final var session = sessionFactory.openSession()) {
            final var tx = session.beginTransaction();
            final var human = session.find(NPlusOneCacheableHuman.class, humanId);
            Assertions.assertEquals(3, human.getPets().size());
            tx.commit();
        }
    }

}
