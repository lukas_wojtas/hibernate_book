package pl.controller_one;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EntityScan(basePackages = {"app"})
@EnableCaching
public class ControllerOneApp {

    public static void main(String[] args) {
        SpringApplication.run(ControllerOneApp.class, args);
    }
}
