package pl.controller_one;

import app.entities.NPlusOneCacheableHuman;
import app.entities.NPlusOneCacheablePet;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cache.jcache.internal.JCacheRegionFactory;
import org.hibernate.cfg.Environment;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.context.WebApplicationContext;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class ControllerOneConfig {

    @Bean
    @Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
    Session getSession(SessionFactory factory) {
        return factory.openSession();
    }

    @Bean
    SessionFactory entityManagerFactory(DataSource dataSource) {
        StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        Map<String, Object> settings = new HashMap<>();
        settings.put(Environment.DRIVER, "com.mysql.cj.jdbc.Driver");
        settings.put(Environment.URL, "jdbc:mysql://localhost:3306/book");
        settings.put(Environment.USER, "lukasz");
        settings.put(Environment.PASS, "meteor1981");
        settings.put(Environment.USE_SECOND_LEVEL_CACHE, true);
        settings.put(Environment.USE_QUERY_CACHE, true);
        settings.put(Environment.CACHE_REGION_FACTORY, JCacheRegionFactory.class);
        settings.put(Environment.GENERATE_STATISTICS, true);
        settings.put(Environment.SHOW_SQL, true);
        settings.put(Environment.FORMAT_SQL, true);
        settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL8Dialect");
        settings.put("hibernate.javax.cache.missing_cache_strategy", "create");
        registryBuilder.applySettings(settings);
        StandardServiceRegistry registry = registryBuilder.build();
        MetadataSources sources = new MetadataSources(registry)
                .addAnnotatedClass(NPlusOneCacheablePet.class)
                .addAnnotatedClass(NPlusOneCacheableHuman.class);
        Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }
}
